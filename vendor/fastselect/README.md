website: https://dbrekalo.github.io/fastselect/

source: https://github.com/dbrekalo/fastselect

author: Damir Brekalo https://github.com/dbrekalo

MIT license
