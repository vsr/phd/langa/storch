After deleting some named graphs or all graphs from your triple store it can be necessary to re-create the [text index](https://github.com/NatLibFi/Skosmos/wiki/InstallTutorial#creating-a-text-index).

When I was working on the development machine I deleted ALL graphs, because I wanted to start from scratch on the rdf data side (delete the old WIP data and import new terminologies).
After that I had a **lot of troubles with the search function** in the GUI, that is heavily dependend on the triple store and the text index (if you use one).

![iconfinder_warning.png](../resource/pics/iconfinder_101_Warning_183416.png) use with care - this will delete the whole text index for Fuseki

These were the steps to get the search function working again (compare [Skosmos text index tutorial](https://github.com/NatLibFi/Skosmos/wiki/InstallTutorial#creating-a-text-index)):
1. `sudo service fuseki stop`
2. ![iconfinder_warning.png](../resource/pics/iconfinder_101_Warning_183416.png) `rm -r /var/lib/fuseki/databases/skosmos/text` (this will delete the whole text index)
3. `mkdir /var/lib/fuseki/databases/skosmos/text`
4. make sure `/etc/fuseki/configuration/skosmos.ttl` has the correct content, according to [Skosmos text index tutorial](https://github.com/NatLibFi/Skosmos/wiki/InstallTutorial#creating-a-text-index)
5. `sudo service fuseki start`

After that you can load your RDF data again, see: [README.md](../README.md)