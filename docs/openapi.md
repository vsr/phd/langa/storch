## How to create a OpenAPI (Swagger) REST API documentation:

`brew install composer` (for MacOS)

#### Install swagger-php 
https://github.com/zircote/swagger-php

#### Annotate the php functions
compare https://github.com/zircote/swagger-php/ & https://github.com/zircote/swagger-php/blob/master/Examples 
compare rest.php & controller/RestController.php

#### create api documentation (see `./vendor/bin/openapi --help`)
`./vendor/bin/openapi --format json --output openapi.json rest.php controller/RestController.php`
rest.php & controller/RestController.php are the targeted source files, which should have the OA (OpenAPI) annotations

create both json & yaml:
`./vendor/bin/openapi --format yaml --output openapi.yaml rest.php controller/RestController.php && ./vendor/bin/openapi --format json --output openapi.json rest.php controller/RestController.php
`