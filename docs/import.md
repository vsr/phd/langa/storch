### make config.ttl writable for apache2 (www-data)
```
find config.ttl -type d -exec chmod 0755 {} \;
find config.ttl -type f -exec chmod 0755 {} \;
sudo chown -R www-data:www-data config.ttl

# you can also create a special group and make the owners of that file, e.g.
groupadd tisafusers
gpasswd -a www-data tisafusers
gpasswd -a bob tisafusers   # if you have a user named bob
sudo chown -R :tisafusers config.ttl
```

### Hints for importing RDF/SKOS data via TISAF (/import) & easyrdf lib

0. `application/rdf+xml` >> `text/turtle`. Please do not use ttl for larger terminologies, the used parser (EasyRDF) takes lots of time to parse turtle.
For **faster import** use file.rdf (mime type `application/rdf+xml`) instead of Turtle (.ttl) - XML is faster to parse, TTL better to read ;)
To check the mime type on debian you can use `mimetype -a file.rdf`

If you still want to use Turtle, read this:

1. **Turtle** (may also apply for other serialisation formats): take care of **subject names / identifiers** for rdf triples
- e.g. `:APACHE2.0` (not valid), a dot (`'.'`) seems to be not a valid character in a ttl triple subject.
tip: you can find these occurrences in a .ttl file with `^(\w+\.\d)` or `^(\w+\.\w)` regex, e.g. in the [rdflicense terminology](../resource/terminologies/rdf-license/rdflicense-skos-orig.ttl) with the regex `^:(\w+\.\d)`

- the hyphen symbol (`-`) also can be problematic (e.g. `cc-by-1.0`)

- subject name syntax: using **just the underscore** (`_`) as delimiter is valid  and easy

!!! **Attention**: make sure to not break the URI/identifiers of triple subjects! Escaping would be the better way, see below:

> reserved character escape sequences consist of a '\\' followed by one of ~.-!$&'()*+,;=/?#@%_ and represent the character to the right of the '\\'.

see https://www.w3.org/TR/turtle/#sec-escapes

Example. `:APACHE2.0` (not valid) -> `:APACHE2\.0` (valid)

2. **Turtle** labels
- for very long labels use multiple row syntax ("""), e.g.
```
skos:prefLabel """The Creative Commons Rights Expression Language (CC REL) 
    lets you describe copyright licenses in RDF. For more information on 
    describing licenses in RDF and attaching those descriptions to digital 
    works, see CC REL in the Creative Commons wiki."""@en ;

```
instead of one single very long row:
`"skos:prefLabel "The Creative Commons Rights Expression Language (CC REL) lets you describe copyright licenses in RDF. For more information on describing licenses in RDF and attaching those descriptions to digital works, see CC REL in the Creative Commons wiki."@en .`

why? because there is a special skos syntax for these cases and to prevent presentation issues in the web gui


#### more hints for valid rdf syntax (EasyRDF)
see https://www.w3.org/TR/turtle/#sec-escapes, https://w3c.github.io/rdf-tests/turtle/

Recommendation (escape -, .):

- prefx:x-y prop object -> \<prefix:x-y\> prop object 
- prefx:x.y prop object -> \<prefix:x.y\> prop object