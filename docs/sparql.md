### SPARQL queries which I found helpful when working with fuseki/skosmos
Some of these queries where also re-used in the implementation part of TISAF.

#### select all skos:prefLabels from a named graph
```
SELECT DISTINCT ?s ?p ?o
FROM <loc:menc:png2> # graph name
WHERE {
    ?s ?p ?o .
    ?s skos:prefLabel ?o
}
```


#### add statement to existing concept (mencpng/gif) -
```
INSERT DATA
{ GRAPH <loc:menc:png2> {
    <http://id.loc.gov/vocabulary/mencformat/gif> 
    owl:sameAs
   <http://localhost/tisaf/locmpng/gif> # object syntax alternative: use prefix:identifier, e.g. tt:domains
}}
```

#### get all tag labels from named graph
```
SELECT DISTINCT ?s ?o
FROM <<http://example.com/tisaf/tags-v3>> # graph name
WHERE {
    ?s skos:prefLabel ?o
}
```

#### get all p,o from uri (here a subject of type skos:ConceptScheme)
```
SELECT DISTINCT ?p ?o
FROM <loc:menc:png2> # graph name
WHERE {
    <http://id.loc.gov/vocabulary/mencformat> ?p ?o
}
```

#### insert tag "general" to tags terminology ("<http://example.com/tisaf/tags/v4>")
```
INSERT DATA
{ GRAPH <http://example.com/tisaf/tags/v4> {
    <http://vsrstud01.informatik.tu-chemnitz.de/tisaf/tags/tags> 
    <http://vsrstud01.informatik.tu-chemnitz.de/tisaf/hasTags> 
    <http://vsrstud01.informatik.tu-chemnitz.de/tisaf/tags/general> # new terminology scheme
}}
```

#### insert economic sciences tag to stw terminology
```
INSERT DATA {
GRAPH <http://zbw.eu/stw/> # graph name
{
    <http://zbw.eu/stw>
    <http://vsrstud01.informatik.tu-chemnitz.de/tisaf/hasTags> 
    <http://vsrstud01.informatik.tu-chemnitz.de/tisaf/tags/econsci> 
}}
```

#### get s, labels from skos:ConceptScheme
```
SELECT DISTINCT ?s ?label
FROM <loc:menc:png2> # graph name
WHERE {
    ?s ?p ?o .
    ?s rdf:type skos:ConceptScheme .
    ?s <http://vsrstud01.informatik.tu-chemnitz.de/tisaf/tag> ?label
}
```
#### get tag name from named graph (tags terminology)
```
SELECT DISTINCT ?tagName
FROM <<http://example.com/tisaf/tags-v3>> # graph name
WHERE {
    <http://vsrstud01.informatik.tu-chemnitz.de/tisaf/tags#encoding> skos:prefLabel ?tagName .
}
```

#### select all graph names
```
SELECT ?g
WHERE {
    graph ?g {}
}
```


#### get all tags from tisaf
```
SELECT DISTINCT ?s ?label
FROM <<http://example.com/tisaf/tags-v3>>
WHERE{
    ?s a skos:Concept .
    ?s skos:prefLabel ?label .
    FILTER (lang(?label) = 'en')
}
```

#### get all top concepts from namend graph
```
SELECT DISTINCT ?s ?label
FROM <http://example.com/tisaf/tags/v4>
WHERE{
    ?s a skos:Concept .
    ?s skos:topConceptOf ?topConcept .
    ?s skos:prefLabel ?label .
    FILTER (lang(?label) = 'en')
}
```

#### DROP named graph
see https://www.w3.org/TR/sparql11-update/#drop

(need to do that on /skosmos/, not /skosmos/sparql)

```
DROP SILENT GRAPH <graph>
```

#### DROP all graphs
```
DROP SILENT ALL
```

#### get all entered terms from evaluation #2
```
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX dc: <http://purl.org/dc/terms/>

SELECT *
FROM <http://tisaf.de/missing/terms/graph/> #graphname
WHERE {
  ?s skos:prefLabel ?prefLabel .
  ?s dc:date ?date .
  ?s a skos:Concept .
  ?s dc:date ?date .
  ?s skos:note ?reason .
  FILTER(?date >= "2020-08-19" && ?date <= "2020-09-04" ) .
  ?s skos:prefLabel ?value .
  ?s skos:altLabel ?attribute
}
```