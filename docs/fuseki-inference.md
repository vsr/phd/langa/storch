based on the tutorial from https://github.com/jfmunozf/Jena-Fuseki-Reasoner-Inference/wiki/Configuring-Apache-Jena-Fuseki-2.4.1-inference-and-reasoning-support-using-SPARQL-1.1:-Jena-inference-rules,-RDFS-Entailment-Regimes-and-OWL-reasoning

on a Linux machine, following the [InstallationTutorial from Skosmos](https://github.com/NatLibFi/Skosmos/wiki/InstallTutorial), the mentioned directories are at other places:

`/etc/fuseki/configuration` instead of `C:\apache-jena-fuseki-2.4.1\run\configuration\`

`/etc/fuseki/databases` instead of `C:\apache-jena-fuseki-2.4.1\run\databases\`

the fuseki installation uses a special RDFDataset type:
```
text:TextDataset
    rdfs:subClassOf  ja:RDFDataset .
```

1. Create ElQuijote.ttl at `/etc/fuseki/configuration` and adapt paths, like e.g.:
```
@prefix :      <http://base/#> .
@prefix tdb:   <http://jena.hpl.hp.com/2008/tdb#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix ja:    <http://jena.hpl.hp.com/2005/11/Assembler#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix fuseki: <http://jena.apache.org/fuseki#> .

:service1       a                         fuseki:Service ;
        fuseki:dataset                    :dataset ;
        fuseki:name                       "ElQuijote" ;
        fuseki:serviceQuery               "query" , "sparql" ;
        fuseki:serviceReadGraphStore      "get" ;
        fuseki:serviceReadWriteGraphStore "data" ;
        fuseki:serviceUpdate              "update" ;
        fuseki:serviceUpload              "upload" .

:dataset        rdf:type        ja:RDFDataset ;
    rdfs:label "ElQuijote" ;
    ja:defaultGraph
      [ rdfs:label "ElQuijote" ;
        a ja:InfModel ;

        #Reference to model.ttl file
        ja:content [ja:externalContent <file:////etc/fuseki/databases/ElQuijote/model.ttl> ] ;

        #Reference to data.ttl file
        ja:content [ja:externalContent <file:////etc/fuseki/databases/ElQuijote/data.ttl> ] ;

        ja:reasoner [
            ja:reasonerURL <http://jena.hpl.hp.com/2003/GenericRuleReasoner> ;
            ja:rulesFrom <file:////etc/fuseki/databases/ElQuijote/myrules.rules> ;
        ] ;
      ] ;
     .

```

2. copy the files model.ttl, data.ttl, myrules.rules to /etc/fuseki/databases/ElQuijote
3. `sudo service fuseki restart`

### Adaption to owl:sameAs example
- Import [ERO](https://bioportal.bioontology.org/ontologies/ERO/) & [OBI](https://bioportal.bioontology.org/ontologies/OBI/) terminology (they both have the concept 'microscope')
- Define owl:sameAs relationship (concept = ero:microscope owl:sameAs obi:microscope)

Since in TISAF all terminologies are split in separate graphs, we cannot use the straightforward way.
As a **workaround**, we can use the **TISAF-URIs** to **build such relationships between different graphs**.
Because in case of ERO & OBI, both subjects have the same original identifier: `http://purl.obolibrary.org/obo/OBI_0400169`

```
INSERT DATA
{ GRAPH <http://example.com/ero4> { # ero graph name (e.g. http://example.com/ero4, https://bioportal.bioontology.org/ontologies/OBI3)
    <http://purl.obolibrary.org/obo/OBI_0400169>  # microscope subject uri (e.g. http://purl.obolibrary.org/obo/OBI_0400169)
    owl:sameAs
   <http://localhost/tisaf/obi/microscope>  # obi microscope subject TISAF-URI (e.g. http://localhost/tisaf/obi/microscope (! also http://purl.obolibrary.org/obo/OBI_0400169))
}}

# 2
INSERT DATA
{ GRAPH <https://bioportal.bioontology.org/ontologies/OBI3> { # OBI graph name (e.g. http://example.com/ero4, https://bioportal.bioontology.org/ontologies/OBI3)
    <http://purl.obolibrary.org/obo/OBI_0400169>  # microscope subject uri (e.g. http://purl.obolibrary.org/obo/OBI_0400169)
    owl:sameAs
   <http://localhost/tisaf/ero4/microscope>  # ero microscope subject TISAF-URI (e.g. http://localhost/tisaf/ero4/microscope (! also http://purl.obolibrary.org/obo/OBI_0400169))
}}
```

- API call (narrower concepts of obi:microscope)
`http://localhost/tisaf/rest/v1/obi/narrower?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FOBI_0400169&format=application/json`
-> 6 concepts from OBI + (MERGE) 17 concepts from ERO = 17 (the 6 concepts are already in ERO, so 11 additional concepts get returned!)