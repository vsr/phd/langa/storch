<?php

require_once 'vendor/autoload.php';
require_once 'controller/TisafUriHelper.php';
header("Access-Control-Allow-Origin: *"); // enable CORS for the whole REST API

/**
 * this is script to check duplicates in tisaf:uris for skos:concepts of a terminology
 */

try {
    $terminologyId = "obi";
    $config = new GlobalConfig();
    $model = new Model($config);
    $request = new Request($model);
    $request->setVocab($terminologyId);
    $sparql = $request->getVocab()->getSparql();
    $graphName = $request->getVocab()->getGraph();
    $tisafHost = $config->getFullSystemHost();


    // collect tisaf:uri for concepts
    $tisafUriProperty = $model->getConfig()->getTisafPrefix() . "uri";
    $queryString = <<<EOD
SELECT *
FROM <$graphName>
WHERE {
    ?s a skos:Concept .
    ?s <$tisafUriProperty> ?uri
}
EOD;

    // exec. query (read)
    $result = $sparql->execute($queryString);
    $uris = [];
    $uniquePerSubject = [];
    foreach ($result as $item) {
        $uri = $item->uri->getUri();
        $uris[] = $uri;
        $uniquePerSubject[$item->s->getUri()] = $uri;
    }
    $uniqueGlobal = array_unique($uris);

    // counts for output
    $uniquePerSubjectCount = count($uniquePerSubject);
    $uniqueGlobalCount = count($uniqueGlobal);
    $urisCount = count($uris);
    echo "uniquePerSubject: $uniquePerSubjectCount / uniqueGlobal: $uniqueGlobalCount  / uris: $urisCount";
} catch (Exception $e) {
    header("HTTP/1.0 500 Internal Server Error");
    echo('ERROR: ' . $e->getMessage());
}