<?php

require_once 'vendor/autoload.php';
require_once 'controller/TisafUriHelper.php';
header("Access-Control-Allow-Origin: *"); // enable CORS for the whole REST API

/**
 * this is a script to check if any tisaf:uris are not dissolvable (4xx, 5xx status)
 */

try {
    $terminologyId = "unescothes";
    $config = new GlobalConfig();
    $model = new Model($config);
    $request = new Request($model);
    $request->setVocab($terminologyId);
    $sparql = $request->getVocab()->getSparql();
    $graphName = $request->getVocab()->getGraph();
    $tisafHost = $config->getFullSystemHost();


    // collect tisaf:uri for concepts
    $tisafUriProperty = $model->getConfig()->getTisafPrefix() . "uri";
    $queryString = <<<EOD
SELECT *
FROM <$graphName>
WHERE {
    ?s a skos:Concept .
    ?s <$tisafUriProperty> ?uri
}
EOD;

    // exec. query (read)
    $result = $sparql->execute($queryString);
    $uris = [];
    $uniquePerSubject = [];
    foreach ($result as $item) {
        $uri = $item->uri->getUri();
        $uris[] = $uri;
        $uniquePerSubject[$item->s->getUri()] = $uri;
    }
    $uniqueGlobal = array_unique($uris);

    $errors = [];
    foreach ($uniqueGlobal as $uniqueUri) {
        $result = curl_get($uniqueUri, [], [CURLOPT_FOLLOWLOCATION => TRUE]);
        if ($result["status"] != 200 || empty($result["data"])) {
            $errors[] = [$uniqueUri, $result["status"], $result["data"]];
        }
    }
    echo "result: \n\r";
    echo (print_r($result));

} catch (Exception $e) {
    header("HTTP/1.0 500 Internal Server Error");
    echo('ERROR: ' . $e->getMessage());
}

/**
 * Send a GET requst using cURL
 * @param string $url to request
 * @param array $get values to send
 * @param array $options for cURL
 */
function curl_get($url, array $get = NULL, array $options = array())
{
    $defaults = array(
        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get),
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => 4
    );

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return ["status" => $status, "data" => $result];
}