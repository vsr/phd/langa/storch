<?php

/**
 * Use Composer autoloader to automatically load library classes.
 */

use EasyRdf\Sparql\Result;

try {
    if (!file_exists('./vendor/autoload.php')) {
        throw new Exception('Dependencies managed by Composer missing. Please run "php composer.phar install".');
    }
    require_once 'vendor/autoload.php';
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
    return;
}

$config = new GlobalConfig();
$model = new Model($config);
$controller = new WebController($model);
$request = new Request($model);

// PATH_INFO, for example "/ysa/fi"
$path = $request->getServerConstant('PATH_INFO') ? $request->getServerConstant('PATH_INFO') : '';
$parts = explode('/', $path);


// PoC: vocabName/conceptLabel redirect
// find redirect URI from db (SPARQL), e.g. tisaf/locmpng/XML_file ->
// http://localhost/tisaf/locmpng/en/page/?uri=http%3A%2F%2Fid.loc.gov%2Fvocabulary%2Fmencformat%2Fxml
// TODO: remove support for user_agent=easy rdf client? -> would brake property of stateless server!!!
if (!isSpecialSubpage($path)["bool"] && sizeof($parts) == 3 && empty($parts[0]) && !empty($parts[2])) {
    // check if vocab is really existing/in db (state/sanity check)
    $vocabId = $parts[1];
    try {
        $vocabulary = $model->getVocabulary($vocabId);
    } catch (Exception $e) {
        header("HTTP/1.0 404 Not Found");
        echo ("Error: Terminology with id '$vocabId' not found.");
        return;
    }
    $request->setVocab($vocabId);

    // lang extract from http ref, else default for vocab
    $lang = guessLanguageFromRequest($vocabulary, $request);

    // e.g. http://localhost/tisaf/locmpng/XML_file?format=xyz => remove request parameters
    $requestUri = $request->getServerConstant('REQUEST_URI');
    $requestUri = explode("?", $requestUri)[0];
    $targetUri = $config->getFullSystemHost() . $parts[1] . "/" . urlencode($parts[2]);

    // get subject, where tisaf url is specified with tisaf:uri
    // so we also check if the input conceptId (via REQUEST_URI) is contained in our graph/concept
    $tisafUriProperty = $config->getTisafUriProperty();
    $result = getOriginalIdentifierFromTisafUri($request, $targetUri, $tisafUriProperty);
    if (!$result instanceof Result) {
        header("HTTP/1.0 500 Server Error");
        echo ("Expected result to be of type EasyRDF.Result");
        return;
    }
    if ($result->numRows() < 1) {
        $requestUriError = $request->getServerConstant('REQUEST_URI');
        header("HTTP/1.0 500 Server Error");
        echo ("URL redirect failed for $requestUriError.");
        return;
    }
    $resultUri = $result[0]->subject->getUri();

    // check Accept header and redirect to rest api, if needed
    // negotiate suitable response format -> see EntityController.php
    $restFormats = explode(' ', RestController::TISAF_SUPPORTED_FORMATS);
    $supportedFormats = $restFormats;
    // add HTML as supported format (make it the first element so it becomes default)
    array_unshift($supportedFormats, 'text/html');
    // optional query parameter for forcing a specific format
    $requestedFormat = $request->getQueryParam('format');
    $format = $controller->negotiateFormat($supportedFormats, $request->getServerConstant('HTTP_ACCEPT'), $requestedFormat);
    if (isset($format) && in_array($format, $restFormats)) {
        // rest redirect
        // format param
        $formatUrlParam = !is_null($request->getQueryParam('format')) ? $request->getQueryParam('format') : '';
        $urlParams = ["format" => $formatUrlParam];
        redirectToRestRdf($request, $vocabId, $resultUri, $model, $urlParams);
        return;
    }

    // web redirect
    redirectToHtmlView($request, $vocabId, $lang, $resultUri, $model);
    return;
}

if (sizeof($parts) <= 2) {
    // if language code missing, redirect to guessed language
    // in any case, redirect to <lang>/
    $lang = sizeof($parts) == 2 && $parts[1] !== '' ? $parts[1] : $controller->guessLanguage();
    header("Location: " . $lang . "/");
} else {
  if (array_key_exists($parts[1], $config->getLanguages())) { // global pages
        $request->setLang($parts[1]);
        $content_lang = $request->getQueryParam('clang');
        $request->setContentLang($content_lang);
      $isSpecialSubpage = isSpecialSubpage($path);
      $isSpecialSubpage["bool"] ? $request->setPage($isSpecialSubpage["parts"][2]) : $request->setPage('');
        if ($request->getPage() == '') {
            $controller->invokeVocabularies($request);
        } elseif ($request->getPage() == 'about') {
            $controller->invokeAboutPage($request);
        } elseif ($request->getPage() == 'feedback') {
            $controller->invokeFeedbackForm($request);
        } elseif ($request->getPage() == 'search') {
            $controller->invokeGlobalSearch($request);
        } elseif ($request->getPage() == 'full-search') {
            $controller->renderFullSearchTemplate($request);
        } elseif ($request->getPage() == 'import') {
            $controller->renderImport($request);
        } else {
            $controller->invokeGenericErrorPage($request);
        }
    } else { // vocabulary-specific pages
        $vocab = $parts[1];
        try {
            $request->setVocab($parts[1]);
        } catch (Exception $e) {
            $request->setLang($controller->guessLanguage());
            $controller->invokeGenericErrorPage($request);
            return;
        }
        if (sizeof($parts) == 3) { // language code missing
            $lang = $controller->guessLanguage();
            $newurl = $controller->getBaseHref() . $vocab . "/" . $lang . "/";
            header("Location: " . $newurl);
        } else {
            if (array_key_exists($parts[2], $config->getLanguages())) {
                $lang = $parts[2];
                $content_lang = $request->getQueryParam('clang') ? $request->getQueryParam('clang') : $lang;
                $request->setContentLang($content_lang);
                $request->setLang($parts[2]);
                $request->setPage($parts[3]);
                if (!$request->getPage()) {
                    $request->setPage('vocab');
                    $controller->invokeVocabularyHome($request);
                } elseif ($request->getPage() == 'feedback') {
                    $controller->invokeFeedbackForm($request);
                } elseif ($request->getPage() == 'search') {
                    $controller->invokeVocabularySearch($request);
                } elseif ($request->getPage() == 'index') {
                    if ((sizeof($parts) == 5) && $parts[4] !== '') {
                        // letter given
                        $request->setLetter($parts[4]);
                    }

                    $controller->invokeAlphabeticalIndex($request);
                } elseif ($request->getPage() == 'page') {
                    ($request->getQueryParam('uri')) ? $request->setUri($request->getQueryParam('uri')) : $request->setUri($parts[4]);
                    if ($request->getUri() === null || $request->getUri() === '') {
                        $controller->invokeGenericErrorPage($request);
                    } else {
                        $controller->invokeVocabularyConcept($request);
                    }

                } elseif ($request->getPage() == 'groups') {
                    $controller->invokeGroupIndex($request);
                } elseif ($request->getPage() == 'changes') {
                    $controller->invokeChangeList($request, 'dc:modified');
                } elseif ($request->getPage() == 'new') {
                    $controller->invokeChangeList($request);
                } else {
                    $controller->invokeGenericErrorPage($request);
                }
            } else { // language code missing, redirect to some language version
                $lang = $controller->guessLanguage($vocab);
                $newurl = $controller->getBaseHref() . $vocab . "/" . $lang . "/" . implode('/', array_slice($parts, 2));
                $qs = $request->getServerConstant('QUERY_STRING');
                if ($qs) {
                    $newurl .= "?" . $qs;
                }
                header("Location: $newurl");
            }
        }
    }
}

/**
 * @param Request $request
 * @param string $vocabId
 * @param string $lang
 * @param $resultUri
 * @param Model $model
 */
function redirectToHtmlView(Request $request, string $vocabId, string $lang, $resultUri, Model $model): void
{
// e.g. "http://localhost/tisaf/locmpng/en/page/?uri=" . urlencode("http://id.loc.gov/vocabulary/mencformat/gif");
    $redirectTargetUri = $request->getServerConstant('REQUEST_SCHEME')
        . '://' . $request->getServerConstant('HTTP_HOST') . "/"
        . "$vocabId/$lang/page/?uri=" . urlencode($resultUri);
    $entityController = new EntityController($model);
    $entityController->redirect303($redirectTargetUri);
}

/**
 * @param Vocabulary $vocabulary
 * @param Request $request
 * @return string
 */
function guessLanguageFromRequest(Vocabulary $vocabulary, Request $request): string
{
    $lang = $vocabulary->getConfig()->getDefaultLanguage();
    $vocabId = $vocabulary->getId();
    foreach ($vocabulary->getConfig()->getLanguages() as $vLang) {
        $httpRef = $request->getServerConstant('HTTP_REFERER');
        if ($httpRef != null && str_contains($httpRef, "$vocabId/$vLang/")) {
            $lang = $vLang;
        }
    }
    $lang = $vocabulary->verifyVocabularyLanguage($lang);
    return $lang;
}

/**
 * @param Request $request
 * @param string $targetUri
 * @return \EasyRdf\Graph|\Result
 */
function getOriginalIdentifierFromTisafUri(Request $request, string $targetUri, string $tisafUriProperty)
{
    // !!! property name must be same as in tisafUriMigration.php !!!
    $graph = $request->getVocab()->getGraph();
    $queryString = <<<EOD
SELECT DISTINCT ?subject
FROM <$graph>
WHERE {?subject $tisafUriProperty <$targetUri>}
EOD;
    $result = $request->getVocab()->getSparql()->execute($queryString);
    return $result;
}

/**
 * @param Request $request
 * @param string $vocabId
 * @param $resultUri
 * @param Model $model
 */
function redirectToRestRdf(Request $request, string $vocabId, $resultUri, Model $model, array $urlParams = []): void
{
//http://localhost/tisaf/rest/v1/locmpng/data?uri=http%3A%2F%2Fid.loc.gov%2Fvocabulary%2Fmencformat%2Fjpeg
    $redirectTargetUri = $request->getServerConstant('REQUEST_SCHEME')
        . '://' . $request->getServerConstant('HTTP_HOST') . "/rest/v1/"
        . "$vocabId/data?uri=" . urlencode($resultUri);
    foreach ($urlParams as $key => $value) {
        $redirectTargetUri .= "&$key=$value";
    }
    $entityController = new EntityController($model);
    $entityController->redirect303($redirectTargetUri);
}

// parse http accept header including weighting (q factor)
// see http://jrgns.net/parse_http_accept_header/index.html
// author: Jurgens du Toit
function accept_header($header = false) {
    $result = null;
    $header = $header ? $header : (array_key_exists('HTTP_ACCEPT', $_SERVER) ? $_SERVER['HTTP_ACCEPT']: false);
    if ($header) {
        $types = explode(',', $header);
        $types = array_map('trim', $types);
        foreach ($types as $one_type) {
            $one_type = explode(';', $one_type);
            $type = array_shift($one_type);
            if ($type) {
                list($precedence, $tokens) = accept_header_options($one_type);
                list($main_type, $sub_type) = array_map('trim', explode('/', $type));
                $result[] = array('main_type' => $main_type, 'sub_type' => $sub_type, 'precedence' => (float)$precedence, 'tokens' => $tokens);
            }
        }
        usort($result, array('Parser', 'compare_media_ranges'));
    }
    return $result;
}

function accept_header_options($type_options) {
    $precedence = 1;
    $tokens = array();
    if (is_string($type_options)) {
        $type_options = explode(';', $type_options);
    }
    $type_options = array_map('trim', $type_options);
    foreach ($type_options as $option) {
        $option = explode('=', $option);
        $option = array_map('trim', $option);
        if ($option[0] == 'q') {
            $precedence = $option[1];
        } else {
            $tokens[$option[0]] = $option[1];
        }
    }
    $tokens = count ($tokens) ? $tokens : false;
    return array($precedence, $tokens);
}

function isSpecialSubpage(string $path, bool $ignoreToken = true) {
    $pathCopy = $path;
    if (str_contains($path, "&") && $ignoreToken) {
        // remove anylang=on
        $pathCopy = strtok($path, "&");
    }
    $parts = explode('/', $pathCopy);
    $result = ["bool" => !empty($parts) && sizeof($parts) == 3 &&
        ($parts[2] == 'about' || $parts[2] == 'feedback' || $parts[2] == 'search' ||
            $parts[2] == 'full-search' || $parts[2] == 'import')];
    $result["parts"] = $parts;
    return $result;
}