# TISAF
## Terminology-Service zur interdisziplinären, semantischen Annotation von Forschungsdaten
##### (terminology service for interdisciplinary & semantic annotation of research data)

### Installation/Setup
0. install apache2, clone this repo into /var/www/html
1. change [config.ttl](config.ttl), [resource/js/tisaf.js](resource/js/tisaf.js) constants to your productive host name etc.
2. follow https://github.com/NatLibFi/Skosmos/wiki/InstallTutorial 
(hints: on debian 10, you need to install openjdk-11 instead of 8 seems not to be available out-of-the-box, also `apt install php libapache2-mod-php` and `apt install php7.3-xsl php7.3-intl php7.3-mbstring` need to be used - adapt 7.2 with your php version)

#### Fuseki Security
By default, on http://<host>:3030 the fuseki admin interface is visible, but data can only be read or written by localhost with default config.
To make that double sure, you can start Fuseki with `./fuseki-server --localhost` or `service fuseki start --localhost`.
An even better solution would be to "hide" the host port 3030 completely to the outside world, e.g. with proxying or something similar.

### Import TISAF-specific terminologies
- optional: import [tisaf terminology](resource/terminologies/tisaf.ttl)
- !!! [tisaf tags terminology](resource/terminologies/tisaf-tags-v4-skosified.ttl) -> needed for tagging of terminologies (and optionally also concepts - TODO)

The **tags terminology** is a hard depenecy of the TISAF service, and **should** be imported with terminology shortName (aka Vocabulary-ID) = `tags`.
You need to use the cmd line tools for a new TISAF installation (e.g. `/opt/fuseki/bin/s-put http://localhost:3030/skosmos/data http://www.tisaf.de/tags/ tisaf-tags-v6-skosified.ttl`) or you can use the own TISAF importer  (see below).

### Import external terminologies
1. download rdf(s), owl or other skos file (e.g. .ttl)

2. Transform input data
- a) skos file:
  - use [Skosify](https://github.com/NatLibFi/Skosify/) with e.g. `skosify file.owl --o output.ttl --label "my terminology lalbel"` (or rdf/xml file as input etc.)
- b) owl/rdf/rdfs file:
  - use [Skosify](https://github.com/NatLibFi/Skosify/) with e.g. `skosify file.owl -c examples/dctype.cfg --o output.ttl --label "my terminology lalbel"`
- c) MADS file ([Example](http://id.loc.gov/vocabulary/mencformat.html))
  - use [rdfonto2skos](https://github.com/matthiastz/rdfonto2skos) (fork from Skosify) with e.g. `python skosify.py file.owl -c examples/mads2skos.cfg --o output.ttl --label "my terminology lalbel"`

3. Load data: 
- a) via cmd line
  - info [here](https://github.com/NatLibFi/Skosmos/wiki/InstallTutorial#load-data)
  - cmd: `/opt/fuseki/bin/s-put http://localhost:3030/skosmos/data <uri> output.ttl`
- b) via Fuseki Web GUI (localhost:3030)
  - go to [Fuseki Web GUI](http://localhost:3030/dataset.html?tab=upload&ds=/skosmos)
  - select graph name & input ttl file
  - click upload
- c) via TISAF Web GUI (http://example.com/tisaf/import)
 - follow the import form
 - **recommended**: use [Skosify](https://github.com/NatLibFi/Skosify/) for your input skos rdf file
 - for **faster import** use file.rdf (mime type `application/rdf+xml`) instead of Turtle (.ttl) - XML is faster to parse, TTL better to read ;)
 - select & upload that skos rdf file
 - you can skip the next step (4. config.ttl), but make sure config.ttl is writable by apache2/TISAF: [see import.md](docs/import.md)

You will now have inserted your terminology in your Fuseki triple store as a separate graph, but won't see any data in the GUI nor can you query data via the rest api.
So you have to apply the following configuration step:

4. change config.ttl to add an entry for the new terminology, e.g.: (minimum)
```
:terminology-id a skosmos:Vocabulary, void:Dataset ;
    dc:title "my terminology title"@en ;
    skosmos:shortName "terminology short name (used for gui dropdown)";
    dc:subject :category_name ;
    void:uriSpace "uri";
    skosmos:language "en";
    skosmos:defaultLanguage "en";
    void:sparqlEndpoint <http://localhost:3030/skosmos/sparql> ; # adapt to your sparql endpoint
    skosmos:sparqlGraph <uri> .
```

#### tisaf uri migration
We now have imported external data into our TISAF app/namespace. In order to build up **own TISAF specific internal identifiers** in the form of
URIs, you have 2 options:

1. use `/import` (Web GUI preferred) for the whole import process (contenxt: see above steps), the tisaf uri migration will be done fully automatically!
(see [RestController import function](./controller/RestController.php))

2. use the  the [tisafUriMigration script](./tisafUriMigration.php) manually (set the correct terminology id parameter there)


### Maximum file size
PHP will upload the given input files from /import to a temporary dir/file.
In order to get the import functionality fully working, adapt the `upload_max_filesize` attribute in your php.ini config file.
E.g.:
``` 
upload_max_filesize = 25M # original: 2M
post_max_size = 30M # post_max should be a bit bigger; original: 8M
```
Sync the values between the frontend part in [tisaf.js](resource/js/tisaf.js) and your php.ini.
And do not forget to: `sudo service apache2 restart`.

Btw, this can also be the fix if you get the error `Input file was not uploaded correctly. Please check the name, size and file type...`

### Adding/changing translations ([see GitHub doc](https://github.com/NatLibFi/Skosmos/wiki/Translation))

Example: you have a new translation in **lang=de**, with following data:
```
# save that content in resource/translations/skosmos_de.po
msgid "Extended Search" # id of the translation
msgstr "Erweiterte Suche" # concrete value
```

Then you have to transform the data, so it gets read by the app:

```
cd resource/translations
sh trans_script
```

In case you have problems with access rights on the files, then you may want to change that too. See [docs/import.md](docs/import.md) for an example.

----

For more documentation please lookup [/docs](./docs).