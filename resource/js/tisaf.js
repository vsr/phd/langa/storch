// TODO: ADAPT API URI for productive system
const API = 'http://vsrstud01.informatik.tu-chemnitz.de/rest/v1';
// see @prefix tt: terminologies/tisaf-tags*.ttl
const ttPrefix = "http://vsrstud01.informatik.tu-chemnitz.de/tags/";
// TODO: n3, n-triples would be also possible. see PHP Easy RDF Format.php
const allowedRdfFileTypes =
    ['application/rdf+xml', 'text/turtle', 'application/ld+json', 'application/json', 'text/plain', 'application/n-triples']

const sourceInput = $("input[name='source']");
const vocabIdInput = $("input[name='terminologyID']");

$.Fastselect.defaults.placeholder = 'Select tags'

$(document).ready(
    // fast select init
    $('.multipleSelect').fastselect(),

    $('#match-input li').click(function(event) {
      $('#match-input li').removeClass('match-selected');
      $(this).addClass('match-selected');
      // https://stackoverflow.com/a/32922725
      // event.stopPropagation();
    }),

  // workaround to trigger search by input enter
  $("#full-search").keypress(function(event) {
    // enter
    if ("full-search" === event.target.id &&
        (event.key === "Enter" || event.keyCode === 13 || event.charCode === 13)) {
      $("#searchBtn").click();
    }
  }),

  // import form
  $("form#importForm").submit((event) => {
    event.preventDefault(); // prevent form submission behaviour -> use custom post request (function)
    submitImportForm();
  }),

  // source uri validation -> later be used as graph name and uri space
    sourceInput.focusout(() => {
      if (sourceInput.val().trim().length > 0) {
        $.get(`${API}/graphs`).done(function(data) {
          if (data.length > 0 && data.some(graphName => graphName === sourceInput.val().trim())) {
            const txt = 'Identifier already in use. Please provide a new, unique URI or identifier.'
            console.error(txt);
            alert(txt);
            sourceInput.focus();
          }
        })
      }
    }),

    // vocabID validation
    vocabIdInput.focusout(() => {
          const vocabId = vocabIdInput.val().trim();
          const regex = /^([\w\d]{3,12}$)/;
          if (regex.test(vocabId)) {
            $.get(`${API}/terminologyIDs`).done(function(data) {
              if (data.length > 0 && data.some(vocabIdApi => vocabIdApi === vocabId)) {
                const txt = 'Short name already in use. Please provide a new, unique short name (acronym).'
                console.error(txt);
                alert(txt);
                vocabIdInput.focus();
              }
            })
          } else {
            if (vocabId.length > 0) {
              const txt = 'Short name must be alphanumeric and in the length 3-12. Please provide a new value.'
              console.error(txt);
              alert(txt);
              vocabIdInput.focus();
            }
          }
        }
    )
)

function selectAll() {
  $(".checkbox input").prop("checked", true)
}

function selectNone() {
  $(".checkbox input").removeAttr("checked")
}

function extendedSearchRequest() {
  const activeTagInputs = $(".checkbox input:checked");
  const activeTagUris = [];
  for (let i = 0; i < activeTagInputs.length; i++) {
    activeTagUris.push($(activeTagInputs[i]).data('tag-uri'))
  }

  const searchInputText = $("input#full-search").val().trim();

  // construct url + query params
  // - string match style
  const matchStyle = $(".match-selected");
  let queryTerm = `${encodeURI(searchInputText)}*` // default: starts-with
  if (matchStyle && matchStyle.length === 1 && matchStyle.attr("data-match") === 'contains') {
    queryTerm = `*${encodeURI(searchInputText)}*`
  }

  let url = `${API}/search?query=${queryTerm}`
  if (activeTagUris.length > 0) {
    url += `&tags=`
    for (let i = 0; i < activeTagUris.length; i++) {
      const tagFragment = activeTagUris[i].replace(ttPrefix, '')
      url += tagFragment;

      if (i < activeTagUris.length - 1) {
        url += ","
      }
    }
  // tags join style
    const tagsJoinStyle = $("select#tjs").val();
    url += `&tagsjoin=${tagsJoinStyle}`
  }

  // only do API call if input not empty
  if (searchInputText.length > 0) {
    $.get({
      url: url
    }).done(function(data) {
      buildResultCards(data)
    }).complete(function(data) {
      // update ui elements
      $("#searchBtn").removeAttr("disabled");
      $("#loadingGif").css("display", "none");
    })
    $("#searchBtn").attr("disabled", "");
    $("#loadingGif").css("display", "block");
    $("#results-infotxt p").text('Loading search results ...');
  }
}

function toggleTagButton(button) {
  $(button).toggleClass("btn-success btn-outline");
}

function buildResultCards(data) {
  // delete old search result cards.
  const searchResultEl = $("#full-search-results");
  searchResultEl.empty();

  if (data === null || data.length === 0) {
    $("#results-infotxt p").text('0 results found.');
    return;
  }

  const results = data['results'];

  // reset old hint txt
  $("#results-infotxt p").text(`${results.length} results found.`);


  for (let i = 0; i < results.length; i++) {
    const result = results[i];
    //console.log(result)
    const table = $("<table style='text-align: left'>").addClass("");

    // alt label
    const altLabel = createTableRowSearchResult(result["altLabel"], "resource/pics/replaced.gif", "Alternative Term");
    const broader = createTableRowSearchResult(result["skos:broader"], "resource/pics/broader.gif",
        "Broader Terms", true);
    const narrower = createTableRowSearchResult(result["skos:narrower"], "resource/pics/narrower.gif",
        "Narrower Terms", true);
    const otherLangs = createOtherLangsTableRow(result["prefLabelLang"], "resource/pics/info.gif",
        "In other Languages");

    // .. more attributes
    const vocabInfo = $("<tr>").append(
        $("<td style='vertical-align:middle'>").append(
            $("<span>").addClass("glyphicon glyphicon-list-alt property-click").attr("aria-hidden", "true")
                .attr("title", "Terminology title"),

        ),
        $("<td style='vertical-align:middle'>").append(
            $("<a style='margin-left: 5px'>").text(result["vocabTitle"]).attr("href", result['vocabRef'])
        )
    )

    table.append($("<tbody>")).append(vocabInfo, altLabel, broader, narrower, otherLangs);

    // result div
    let conceptLink = result["legacyLink"] || "#";
    if ("tisafUri" in result && result["tisafUri"].length > 0) {
      conceptLink = result["tisafUri"];
    }
    let conceptHeaderText = result["prefLabel"] || result["altLabel"];
    const resultDiv = $("<div style='text-align: center; background: white'>").addClass("result-div").append(
        // concept link
        $("<a style='margin-left: 5px; font-size: 16px; font-weight: bold'>").text(conceptHeaderText).attr("href", conceptLink),
        table
    )

    searchResultEl.append(resultDiv);
  }
}

function createOtherLangsTableRow(data, src, altTerm) {
  let el = null;
  if (data) {
    el = $("<tr>").append(
        $("<td style='vertical-align:middle'>").append(
            //.addClass("property-click")
            $("<span>").attr("aria-hidden", "true").addClass("property-click")
                .attr("title", altTerm)
                .append(
                    $("<img style='vertical-align: middle'>").addClass("property-hover").attr("src", src)
                        .attr("alt", altTerm)
                )
        )
    )

    // langs
    const keys = Object.keys(data)
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const lang = data[key];
      let txt = `${lang} (${key})`;
      if (i < keys.length - 1) {
        txt += ", ";
      }
      el.append(
          $("<span style='margin-left: 5px'>").text(txt)
      )
    }
  }
  return el;
}

function createTableRowSearchResult(attribute, src, altTerm, isMulti = false) {
  let el = null;
  if (attribute && attribute.length > 0) {
    el = $("<tr>").append(
        $("<td style='vertical-align:middle'>").append(
            //.addClass("property-click")
            $("<span>").attr("aria-hidden", "true").addClass("property-click")
                .attr("title", altTerm)
                .append(
                    $("<img style='vertical-align: middle'>").addClass("property-hover").attr("src", src)
                        .attr("alt", altTerm)
                )
        ),
        !isMulti ?
        $("<td style='vertical-align:middle'>").append($("<h5 style='margin-left: 5px'>").text(attribute))
            : createCommaSeparatedSkosAttrTableCell(attribute)

    )
  }
  return el;
}

function createCommaSeparatedSkosAttrTableCell(attribute) {
  const td = $("<td style='vertical-align:middle'>");
  for (let i = 0; i < attribute.length; i++) {
    const uriAttribute = attribute[i];
    let uriValue = uriAttribute["uri"] || "#";
    if ("tisafUri" in uriAttribute) {
      uriValue = uriAttribute["tisafUri"];
    }
    td.append(
        $("<a style='margin-left: 5px'>").text(attribute[i]['label']).attr("href", uriValue)
    )
    if (i < attribute.length - 1) {
      td.append($("<span>").text(", "))
    }
  }

  return td;
}

function submitImportForm() {
  // tag uris
  const selectedTags = $("#tagsSelect").val()

  // blob
  const skosFile = $("input#skosFile")[0].files[0];

  if (!allowedRdfFileTypes.some((allowedType) => allowedType === skosFile.type)) {
    const errorText = `SKOS input file has unsupported file type: ${skosFile.type}!
Allowed types: ${allowedRdfFileTypes}.
Please select another file.`
    console.error(errorText)
    alert(errorText)
    return
  }

  const fileSize = toFixedNum(skosFile.size / 1000 / 1000); // in MB
  const maxFileSize = 25;
  if (fileSize > maxFileSize) {
    const errorText = `SKOS input file has exceeded maximum allowed file size.
Maximum file size: ${maxFileSize} MB, input file size: ${fileSize} MB`
    console.error(errorText)
    alert(errorText)
    return
  }

  // handover form and create form data from it
  const form = $("#importForm");
  const fd = new FormData(form[0]);
  fd.append("selectedTags", selectedTags)
  fd.append("skosFile", skosFile)


  // call rest api. -> POST API/import
  $.post({
    url: `${API}/import`,
    data: fd,
    contentType: false,
    processData: false
    }).done(function(data, textStatus, jqXHR) {
    const badge = $("<div style='padding: 10px'>").addClass("alert-success").text("Success! Terminology was imported.");
    $("#resultStatus").empty().append( badge );
    const link = jqXHR.getResponseHeader("Location");
    if (link !== null && link.length > 0) {
      badge.append(
        $("<br>"),
        $("<a>").attr("href", link).text("Link to new terminology")
      )
    }

  }).error(function (data) {
    console.error(data);
    const responseText = data.responseText || 'Terminology import failed';
    if (isJsonString(responseText) && 'devError' in JSON.parse(responseText)) {
      console.error(JSON.parse(responseText)['devError'])
    }
    if (isJsonString(responseText) && 'userError' in JSON.parse(responseText)) {
      $("#resultStatus").empty().append(
          $("<div style='padding: 10px'>").addClass("alert-danger").text(
              `Terminology import failed: ${JSON.parse(responseText)['userError']}.`
          )
      );
    } else {
      $("#resultStatus").empty().append(
          $("<div style='padding: 10px'>").addClass("alert-danger").text(responseText)
      );
    }
  }).complete(function(data) {
    // update ui elements
    $("#importForm button[type='submit']").removeAttr("disabled");
    $("#loadingGif").css("display", "none");
  })
  $("#importForm button[type='submit']").attr("disabled", "");
  $("#loadingGif").css("display", "inline");
}

function toFixedNum(input) {
  const str = input.toFixed(2);
  return Number(str);
}

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}

function isJsonString(str) {
  try {
    var json = JSON.parse(str);
    return (typeof json === 'object');
  } catch (e) {
    return false;
  }
}
