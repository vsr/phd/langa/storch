<?php

use EasyRdf\GraphStore;
use EasyRDF\RdfNamespace;
use EasyRdf\Resource;
use EasyRdf\Literal;
use EasyRdf\Sparql\Result;

require_once 'controller/TisafUriHelper.php';

/**
 * @OA\Schema(
 *   schema="format",
 *   type="array",
 *   description="Format to serialize a RDF resource",
 *   default="application/rdf+xml",
 *   @OA\Items(
 *       type="string",
 *       enum = {"application/rdf+xml", "text/turtle", "application/ld+json", "application/json"},
 *   )
 * )
 */

/**
 * RestController is responsible for handling all the requests directed to the /rest address.
 */
class RestController extends Controller
{
    /**
     * URI PATH constants
     */
    const REST = 'rest';
    const VERSION = 'v1';
    const VOCABULARIES_PATH = 'vocabularies';
    const DATA_PATH = 'data';

    /* supported MIME types that can be used to return RDF data */
    const SUPPORTED_FORMATS = 'application/rdf+xml text/turtle application/ld+json application/json application/marcxml+xml';
    const TISAF_SUPPORTED_FORMATS = 'application/rdf+xml text/turtle application/ld+json application/json';
    /* context array template */
    const TAGSJOIN_ANY = 'any';
    const TAGSJOIN_EXACT = 'exact';
    private $context = array(
        '@context' => array(
            'skos' => 'http://www.w3.org/2004/02/skos/core#',
            'uri' => '@id',
            'type' => '@type',
        ),
    );

    private static function addTisafUriForConcepts(Vocabulary $voc, $conceptUris, $lang)
    {
        foreach ($conceptUris as $key => $value) {
            $concepts = $voc->getConceptInfo($key, $lang);
            if (sizeof($concepts) == 1) {
                $concept = $concepts[0];

                // tisaf:uri, if existing (see tisafUriMigration)
                $tisafUri = $concept->getTisafUri();
                if (!empty($tisafUri)) {
                    $conceptUris[$key]["tisaf:uri"] = $tisafUri;
                }
            }
        }
        return $conceptUris;
    }

    /**
     * Handles json encoding, adding the content type headers and optional callback function.
     * @param array $data the data to be returned.
     */
    private function returnJson($data)
    {
        // wrap with JSONP callback if requested
        if (filter_input(INPUT_GET, 'callback', FILTER_SANITIZE_STRING)) {
            header("Content-type: application/javascript; charset=utf-8");
            echo filter_input(INPUT_GET, 'callback', FILTER_UNSAFE_RAW) . "(" . json_encode($data) . ");";
            return;
        }

        // otherwise negotiate suitable format for the response and return that
        $negotiator = new \Negotiation\Negotiator();
        $priorities = array('application/json', 'application/ld+json');
        $best = filter_input(INPUT_SERVER, 'HTTP_ACCEPT', FILTER_SANITIZE_STRING) ? $negotiator->getBest(filter_input(INPUT_SERVER, 'HTTP_ACCEPT', FILTER_SANITIZE_STRING), $priorities) : null;
        $format = ($best !== null) ? $best->getValue() : $priorities[0];
        header("Content-type: $format; charset=utf-8");
        header("Vary: Accept"); // inform caches that we made a choice based on Accept header
        echo json_encode($data);
    }

    /**
     * Parses and returns the limit parameter. Returns and error if the parameter is missing.
     */
    private function parseLimit()
    {
        $limit = filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT) ? filter_input(INPUT_GET, 'limit', FILTER_SANITIZE_NUMBER_INT) : $this->model->getConfig()->getDefaultTransitiveLimit();
        if ($limit <= 0) {
            return $this->returnError(400, "Bad Request", "Invalid limit parameter");
        }

        return $limit;
    }


/** Global REST methods **/

    /**
     * Returns all the vocabularies/terminologies available on the server in a json object.
     * @param Request $request
     * @param bool $useTisafUris use custom TISAF uris, e.g. for /terminologies rest api call.
     * @return void
     *
     * @OA\Get(
     *     path="/terminologies",
     *     @OA\Response(response="200", description="List of all terminologies")
     * )
     *
     */
    public function vocabularies(Request $request, bool $useTisafUris = false)
    {
        if (!$request->getLang()) {
            // default language en
            $request->setLang(Model::DEFAULT_LANG);
            //return $this->returnError(400, "Bad Request", "lang parameter missing");
        }

        $this->setLanguageProperties($request->getLang());

        $vocabs = array();
        foreach ($this->model->getVocabularies() as $voc) {
            $vocabs[$voc->getId()] = $voc->getConfig()->getTitle($request->getLang());
        }
        ksort($vocabs);
        $basePath = $this->getBaseHref() . self::REST . "/" . self::VERSION . "/" . self::VOCABULARIES_PATH;
        $tisafBasePath = null;
        if ($useTisafUris) {
            $tisafBasePath = $this->model->getConfig()->getFullSystemHost() . self::REST . "/" . self::VERSION;
            $basePath = $tisafBasePath . "/terminologies";
        }

        $results = array();
        foreach ($vocabs as $id => $title) {
            $results[] = array(
                'id' => $id,
                'uri' => (($useTisafUris) ? $tisafBasePath : $basePath ). "/" . $id,
                'data' => (($useTisafUris) ? $tisafBasePath : $basePath ) . "/" . $id . "/" . (($useTisafUris) ? "export" : "data" ),
                'title' => $title);
        }

        /* encode the results in a JSON-LD compatible array */
        $ret = array(
            '@context' => array(
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'onki' => 'http://schema.onki.fi/onki#',
                'title' => array('@id' => 'rdfs:label', '@language' => $request->getLang()),
                'terminologies' => 'onki:hasVocabulary',
                'id' => 'onki:vocabularyIdentifier',
                'uri' => '@id',
                '@base' => $basePath,
            ),
            'uri' => '',
            'terminologies' => $results,
        );

        return $this->returnJson($ret);
    }

    private function constructSearchParameters($request)
    {
        $parameters = new ConceptSearchParameters($request, $this->model->getConfig(), true);

        $vocabs = $request->getQueryParam('terminologies'); # optional
        // convert to vocids array to support multi-vocabulary search
        $vocids = ($vocabs !== null && $vocabs !== '') ? explode(',', $vocabs) : array();
        $vocabObjects = array();
        foreach($vocids as $vocid) {
            $vocabObjects[] = $this->model->getVocabulary(trim($vocid));
        }
        $parameters->setVocabularies($vocabObjects);
        return $parameters;
    }

    private function transformSearchResults($request, $results, $parameters)
    {
        if (empty($request) || is_null($results)) {
            return $results;
        }

        $fullSystemHost = $this->model->getConfig()->getFullSystemHost();
        // before serializing to JSON, get rid of the Vocabulary object that came with each resource
        foreach ($results as &$res) {

            // TODO: kinda hacky to do that here (add tisaf uri)
            // TODO: but first, the tisafUriMigration script has to be run!
            // (just check if the tisaf:uri is defined on each result concept)
            $res['legacyLink'] = $fullSystemHost . $res['vocab'] . '/' . $res['lang']
                . '/page/?uri=' . urlencode($res['uri']);

            // ... more attributes
            $concepts = $res["voc"]->getConceptInfo($res["uri"], $res["lang"]);
            if (sizeof($concepts) == 1) {
                $concept = $concepts[0];
                $props = $concept->getProperties();
                $vocab = $concept->getVocab();
                $res['vocabTitle'] = $vocab->getTitle();
                // build usable redirect uri
                $res['vocabRef'] = $fullSystemHost .  $vocab->getId();

                $outProps = ['skos:altLabel', 'skos:narrower', 'skos:related', 'skos:broader',
                    "skos:definition", "skos:example"];
                $outProps = ['skos:altLabel', 'skos:narrower', 'skos:related', 'skos:broader', 'skos:definition'];
                foreach ($outProps as $prop) {
                    if (key_exists($prop, $props)) {
                        $res[$prop] = self::getValueFromConceptProp($props[$prop], $res);
                    }
                }

                // other language skos:prefLabels
                $prefLabels = $concept->getForeignLabelList('skos:prefLabel', 'prefLabel');
                foreach ($prefLabels as $prefLabel) {
                    if (sizeof($prefLabel) == 1) {
                        $langLabel = $prefLabel["prefLabel"];
                        if (sizeof($langLabel) == 1) {
                            $res["prefLabelLang"][$langLabel[0]->getLang()] = $langLabel[0]->getLabel();
                        }
                    }
                }

                // tisaf:uri, if existing (see tisafUriMigration)
                $tisafUri = $concept->getTisafUri();
                if (!empty($tisafUri)) {
                    $res["tisafUri"] = $tisafUri;
                }
            }

            // shoot.
            unset($res['voc']);
        }

        $context = array(
            'skos' => 'http://www.w3.org/2004/02/skos/core#',
            'isothes' => 'http://purl.org/iso25964/skos-thes#',
            'onki' => 'http://schema.onki.fi/onki#',
            'uri' => '@id',
            'type' => '@type',
            'results' => array(
                '@id' => 'onki:results',
                '@container' => '@list',
            ),
            'prefLabel' => 'skos:prefLabel',
            'altLabel' => 'skos:altLabel',
            'hiddenLabel' => 'skos:hiddenLabel',
        );
        foreach ($parameters->getAdditionalFields() as $field) {

            // Quick-and-dirty compactification
            $context[$field] = 'skos:' . $field;
            foreach ($results as &$result) {
                foreach ($result as $k => $v) {
                    if ($k == 'skos:' . $field) {
                        $result[$field] = $v;
                        unset($result['skos:' . $field]);
                    }
                }
            }
        }

        $ret = array(
            '@context' => $context,
            'uri' => '',
            'results' => $results
        );

        if (isset($results[0]['prefLabels'])) {
            $ret['@context']['prefLabels'] = array('@id' => 'skos:prefLabel', '@container' => '@language');
        }

        if ($request->getQueryParam('labellang')) {
            $ret['@context']['@language'] = $request->getQueryParam('labellang');
        } elseif ($request->getQueryParam('lang')) {
            $ret['@context']['@language'] = $request->getQueryParam('lang');
        }
        return $ret;
    }

    private function getValueFromConceptProp(ConceptProperty $property, $res) {
        $results = [];
        foreach ($property->getValues() as $key => $value) {
            if ($value instanceof ConceptPropertyValueLiteral) {
                $results[] = $value->getLabel();
            } else if ($value instanceof ConceptPropertyValue) {
                $tisafUri = null;
                $conceptCandidates = $value->getVocab()->getConceptInfo($value->getUri(), "en");
                if (!empty($conceptCandidates) && in_array('skos:Concept', $conceptCandidates[0]->getType())) {
                    $concept = $conceptCandidates[0];
                    $tisafUri = $concept->getTisafUri();
                }
                // add data for property
                $label = '';
                if ($value->getLabel() instanceof Literal) {
                    $label = $value->getLabel()->getValue();
                } elseif (is_string($value->getLabel())) {
                    $label = $value->getLabel();
                    error_log($label);
                }
                $array = ['label' => $label, 'uri' =>
                    // TODO: still legacy link!
                    $this->model->getConfig()->getFullSystemHost() . $res['vocab'] . '/' . $res['lang']
                    . '/page/?uri=' . urlencode($value->getUri())];
                if ($tisafUri !== null) {
                    $array["tisafUri"] = $tisafUri;
                }
                $results[] = $array;
            }
        }
        return $results;
    }

    /**
     * Performs the search function calls. And wraps the result in a json-ld object.
     * @param Request $request
     *
     *  @OA\Get(
     *     path="/search",
     *     @OA\Response(response="200", description="Search results list"),
     *     @OA\Parameter(
     *          name="query",
     *          in="query",
     *          required=true,
     *          description="Search term. It's recommended to use wildcards like query* or *query*",
     *          @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *          name="tags",
     *          in="query",
     *          description="Comma separated list of tags (conceptIds)",
     *          @OA\Schema(
     *              type="string",
     *              example="Biology,Economics,device"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="tagsjoin",
     *          in="query",
     *          description="Tags combination style",
     *          @OA\Schema(
     *             type="array",
     *             default="any",
     *             @OA\Items(
     *                 type="string",
     *                 enum = {"any", "exact"}
     *             )
     *         )
     *     ),
     *     @OA\Parameter(
     *          name="terminologies",
     *          in="query",
     *          description="Comma separated list of terminology IDs to limit search request (filter)",
     *          @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *          name="maxhits",
     *          in="query",
     *          @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *          name="offset",
     *          in="query",
     *          @OA\Schema(type="integer")
     *     )
     * )
     */
    public function search($request)
    {
        $maxhits = $request->getQueryParam('maxhits');
        $offset = $request->getQueryParam('offset');
        $term = $request->getQueryParamRaw('query');

        // tags stuff
        $tags = $request->getQueryParamRaw('tags');
        $vocabsFromTags = null;
        if (!is_null($tags) && !empty($tags)) {
            $tagsArray = explode(',', $tags);

            // tags join method default
            $tagsJoin = self::TAGSJOIN_ANY;
            if (self::TAGSJOIN_EXACT === $request->getQueryParamRaw('tagsjoin')) {
                $tagsJoin = self::TAGSJOIN_EXACT;
            }

            // get from db which concepts are relevant (to be searched for)
            $vocabsFromTags = $this->getRelevantVocabsFromTags($tagsArray, $tagsJoin);
        }

        if (!$term && (!$request->getQueryParam('group') && !$request->getQueryParam('parent'))) {
            return $this->returnError(400, "Bad Request", "query parameter missing");
        }
        if ($maxhits && (!is_numeric($maxhits) || $maxhits <= 0)) {
            return $this->returnError(400, "Bad Request", "maxhits parameter is invalid");
        }
        if ($offset && (!is_numeric($offset) || $offset < 0)) {
            return $this->returnError(400, "Bad Request", "offset parameter is invalid");
        }

        $parameters = $this->constructSearchParameters($request);

        // override vocabs here, if specified by tag input
        // if vocab & tags is set: intersection OR prioritization
        if (!is_null($vocabsFromTags)) {
            $parameters->setVocabularies($vocabsFromTags);
        }

        $results = $this->model->searchConcepts($parameters);
        $ret = $this->transformSearchResults($request, $results, $parameters);

        return $this->returnJson($ret);
    }

    /**
     * @param $tagsFragments array of tags fragments, e.g. 'encoding' or 'license'
     * @param $tagsJoin string tag join method: and, or
     */
    public function getRelevantVocabsFromTags($tagsFragments, $tagsJoin) {
        $fullFragments = [];
        foreach ($tagsFragments as $tag) {
            $fullFragments[] = $this->model->getConfig()->getTisafTagsPrefix() . $tag;
        }

        $vocabsToSearch = [];
        foreach ($this->model->getVocabularies() as $vocab) {
            $tagsForVocab = self::getAllTagsForVocabulary($vocab, true);
            // any/exact match
            if ($tagsJoin == self::TAGSJOIN_ANY && array_intersect($tagsForVocab, $fullFragments)) {
                // match found, can add vocab and skip loop iteration
                $vocabsToSearch[] = $vocab;
                continue;
            } elseif ($tagsJoin == self::TAGSJOIN_EXACT && sizeof($tagsForVocab) == sizeof($fullFragments) &&
                sizeof(array_intersect($tagsForVocab, $fullFragments)) == sizeof($fullFragments)) {
                // exact match
                $vocabsToSearch[] = $vocab;
                continue;
            }
        }


        // select vocabs with sparql query...
        return $vocabsToSearch;
    }

/** Vocabulary-specific methods **/

    /**
     * Loads the vocabulary metadata. And wraps the result in a json-ld object.
     * @param Request $request
     *
     * @OA\Get(
     *     path="/{terminologyId}",
     *      @OA\Parameter(
     *         name="terminologyId",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(response="200", description="Terminology metadata")
     * )
     */
    public function vocabularyInformation($request)
    {
        $vocab = $request->getVocab();
        if ($this->notModified($vocab)) {
            return null;
        }

        /* encode the results in a JSON-LD compatible array */
        $conceptschemes = array();
        foreach ($vocab->getConceptSchemes($request->getLang()) as $uri => $csdata) {
            $csdata['uri'] = $uri;
            $csdata['type'] = 'skos:ConceptScheme';
            $fullSystemHost = $this->model->getConfig()->getFullSystemHost();
            $csdata["tisafUri"] = $fullSystemHost . $vocab->getId();
            $conceptschemes[] = $csdata;
        }

        $ret = array(
            '@context' => array(
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'skos' => 'http://www.w3.org/2004/02/skos/core#',
                'onki' => 'http://schema.onki.fi/onki#',
                'dct' => 'http://purl.org/dc/terms/',
                'uri' => '@id',
                'type' => '@type',
                'conceptschemes' => 'onki:hasConceptScheme',
                'id' => 'onki:vocabularyIdentifier',
                'defaultLanguage' => 'onki:defaultLanguage',
                'languages' => 'onki:language',
                'label' => 'rdfs:label',
                'prefLabel' => 'skos:prefLabel',
                'title' => 'dct:title',
                '@language' => $request->getLang(),
                '@base' => $this->getBaseHref() . self::REST . "/" . self::VERSION . "/" . $vocab->getId() . "/",
            ),
            'uri' => '',
            'id' => $vocab->getId(),
            'marcSource' => $vocab->getConfig()->getMarcSourceCode($request->getLang()),
            'title' => $vocab->getConfig()->getTitle($request->getLang()),
            'defaultLanguage' => $vocab->getConfig()->getDefaultLanguage(),
            'languages' => array_values($vocab->getConfig()->getLanguages()),
            'conceptschemes' => $conceptschemes,
        );

        if ($vocab->getConfig()->getTypes($request->getLang())) {
            $ret['type'] = $vocab->getConfig()->getTypes($request->getLang());
        }

        return $this->returnJson($ret);
    }

    /**
     * Loads the vocabulary metadata. And wraps the result in a json-ld object.
     * @param Request $request
     */
    public function vocabularyStatistics($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $this->setLanguageProperties($request->getLang());
        $arrayClass = $request->getVocab()->getConfig()->getArrayClassURI();
        $groupClass = $request->getVocab()->getConfig()->getGroupClassURI();
        $vocabStats = $request->getVocab()->getStatistics($request->getQueryParam('lang'), $arrayClass, $groupClass);
        $types = array('http://www.w3.org/2004/02/skos/core#Concept', 'http://www.w3.org/2004/02/skos/core#Collection', $arrayClass, $groupClass);
        $subTypes = array();
        foreach ($vocabStats as $subtype) {
            if (!in_array($subtype['type'], $types)) {
                $subTypes[] = $subtype;
            }
        }

        /* encode the results in a JSON-LD compatible array */
        $ret = array(
            '@context' => array(
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'skos' => 'http://www.w3.org/2004/02/skos/core#',
                'void' => 'http://rdfs.org/ns/void#',
                'onki' => 'http://schema.onki.fi/onki#',
                'uri' => '@id',
                'id' => 'onki:vocabularyIdentifier',
                'concepts' => 'void:classPartition',
                'label' => 'rdfs:label',
                'class' => array('@id' => 'void:class', '@type' => '@id'),
                'subTypes' => array('@id' => 'void:class', '@type' => '@id'),
                'count' => 'void:entities',
                '@language' => $request->getLang(),
                '@base' => $this->getBaseHref() . self::REST . "/" . self::VERSION . "/" . $request->getVocab()->getId() . "/",
            ),
            'uri' => '',
            'id' => $request->getVocab()->getId(),
            'title' => $request->getVocab()->getConfig()->getTitle(),
            'concepts' => array(
                'class' => 'http://www.w3.org/2004/02/skos/core#Concept',
                'label' => gettext('skos:Concept'),
                'count' => $vocabStats['http://www.w3.org/2004/02/skos/core#Concept']['count'],
            ),
            'subTypes' => $subTypes,
        );

        if (isset($vocabStats['http://www.w3.org/2004/02/skos/core#Collection'])) {
            $ret['conceptGroups'] = array(
                'class' => 'http://www.w3.org/2004/02/skos/core#Collection',
                'label' => gettext('skos:Collection'),
                'count' => $vocabStats['http://www.w3.org/2004/02/skos/core#Collection']['count'],
            );
        } else if (isset($vocabStats[$groupClass])) {
            $ret['conceptGroups'] = array(
                'class' => $groupClass,
                'label' => isset($vocabStats[$groupClass]['label']) ? $vocabStats[$groupClass]['label'] : gettext(EasyRdf\RdfNamespace::shorten($groupClass)),
                'count' => $vocabStats[$groupClass]['count'],
            );
        } else if (isset($vocabStats[$arrayClass])) {
            $ret['arrays'] = array(
                'class' => $arrayClass,
                'label' => isset($vocabStats[$arrayClass]['label']) ? $vocabStats[$arrayClass]['label'] : gettext(EasyRdf\RdfNamespace::shorten($arrayClass)),
                'count' => $vocabStats[$arrayClass]['count'],
            );
        }

        return $this->returnJson($ret);
    }

    /**
     * Loads the vocabulary metadata. And wraps the result in a json-ld object.
     * @param Request $request
     */
    public function labelStatistics($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $lang = $request->getLang();
        $this->setLanguageProperties($request->getLang());
        $vocabStats = $request->getVocab()->getLabelStatistics();

        /* encode the results in a JSON-LD compatible array */
        $counts = array();
        foreach ($vocabStats['terms'] as $proplang => $properties) {
            $langdata = array('language' => $proplang);
            if ($lang) {
                $langdata['literal'] = Punic\Language::getName($proplang, $lang);
            }

            $langdata['properties'] = array();
            foreach ($properties as $prop => $value) {
                $langdata['properties'][] = array('property' => $prop, 'labels' => $value);
            }
            $counts[] = $langdata;
        }

        $ret = array(
            '@context' => array(
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'skos' => 'http://www.w3.org/2004/02/skos/core#',
                'void' => 'http://rdfs.org/ns/void#',
                'void-ext' => 'http://ldf.fi/void-ext#',
                'onki' => 'http://schema.onki.fi/onki#',
                'uri' => '@id',
                'id' => 'onki:vocabularyIdentifier',
                'languages' => 'void-ext:languagePartition',
                'language' => 'void-ext:language',
                'properties' => 'void:propertyPartition',
                'labels' => 'void:triples',
                '@base' => $this->getBaseHref() . self::REST . "/" . self::VERSION . "/" . $request->getVocab()->getId() . "/",
            ),
            'uri' => '',
            'id' => $request->getVocab()->getId(),
            'title' => $request->getVocab()->getConfig()->getTitle($lang),
            'languages' => $counts,
        );

        if ($lang) {
            $ret['@context']['literal'] = array('@id' => 'rdfs:label', '@language' => $lang);
        }

        return $this->returnJson($ret);
    }

    /**
     * Loads the vocabulary type metadata. And wraps the result in a json-ld object.
     * @param Request $request
     */
    public function types($request)
    {
        $vocid = $request->getVocab() ? $request->getVocab()->getId() : null;
        if ($vocid === null && !$request->getLang()) {
            return $this->returnError(400, "Bad Request", "lang parameter missing");
        }
        if ($this->notModified($request->getVocab())) {
            return null;
        }

        $this->setLanguageProperties($request->getLang());

        $queriedtypes = $this->model->getTypes($vocid, $request->getLang());

        $types = array();

        /* encode the results in a JSON-LD compatible array */
        foreach ($queriedtypes as $uri => $typedata) {
            $type = array_merge(array('uri' => $uri), $typedata);
            $types[] = $type;
        }

        $base = $request->getVocab() ? $this->getBaseHref() . self::REST . "/" . self::VERSION . "/" .
            $request->getVocab()->getId() . "/" : $this->getBaseHref() . self::REST . "/" . self::VERSION . "/";

        $ret = array_merge_recursive($this->context, array(
            '@context' => array(
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'onki' => 'http://schema.onki.fi/onki#',
                'label' => 'rdfs:label',
                'superclass' => array('@id' => 'rdfs:subClassOf', '@type' => '@id'),
                'types' => 'onki:hasType',
                '@language' => $request->getLang(),
                '@base' => $base,
            ),
            'uri' => '',
            'types' => $types)
        );

        return $this->returnJson($ret);
    }

    private function findLookupHits($results, $label, $lang)
    {
        $hits = array();
        // case 1: exact match on preferred label
        foreach ($results as $res) {
            if ($res['prefLabel'] == $label) {
                $hits[] = $res;
            }
        }
        if (sizeof($hits) > 0) return $hits;

        // case 2: case-insensitive match on preferred label
        foreach ($results as $res) {
            if (strtolower($res['prefLabel']) == strtolower($label)) {
                $hits[] = $res;
            }
        }
        if (sizeof($hits) > 0) return $hits;

        if ($lang === null) {
            // case 1A: exact match on preferred label in any language
            foreach ($results as $res) {
                if ($res['matchedPrefLabel'] == $label) {
                    $res['prefLabel'] = $res['matchedPrefLabel'];
                    unset($res['matchedPrefLabel']);
                    $hits[] = $res;
                }
            }
            if (sizeof($hits) > 0) return $hits;

            // case 2A: case-insensitive match on preferred label in any language
            foreach ($results as $res) {
                if (strtolower($res['matchedPrefLabel']) == strtolower($label)) {
                    $res['prefLabel'] = $res['matchedPrefLabel'];
                    unset($res['matchedPrefLabel']);
                    $hits[] = $res;
                }
            }
            if (sizeof($hits) > 0) return $hits;
        }

        // case 3: exact match on alternate label
        foreach ($results as $res) {
            if (isset($res['altLabel']) && $res['altLabel'] == $label) {
                $hits[] = $res;
            }
        }
        if (sizeof($hits) > 0) return $hits;


        // case 4: case-insensitive match on alternate label
        foreach ($results as $res) {
            if (isset($res['altLabel']) && strtolower($res['altLabel']) == strtolower($label)) {
                $hits[] = $res;
            }
        }
        if (sizeof($hits) > 0) return $hits;

        return $hits;
    }

    private function transformLookupResults($lang, $hits)
    {
        if (sizeof($hits) == 0) {
            // no matches found
            return;
        }

        // found matches, getting rid of Vocabulary objects
        foreach ($hits as &$res) {
            unset($res['voc']);
        }

        $ret = array_merge_recursive($this->context, array(
            '@context' => array('onki' => 'http://schema.onki.fi/onki#', 'results' => array('@id' => 'onki:results'), 'prefLabel' => 'skos:prefLabel', 'altLabel' => 'skos:altLabel', 'hiddenLabel' => 'skos:hiddenLabel'),
            'result' => $hits)
        );

        if ($lang) {
            $ret['@context']['@language'] = $lang;
        }

        return $ret;
    }

    /**
     * Used for finding terms by their exact prefLabel. Wraps the result in a json-ld object.
     * @param Request $request
     */
    public function lookup($request)
    {
        $label = $request->getQueryParamRaw('label');
        if (!$label) {
            return $this->returnError(400, "Bad Request", "label parameter missing");
        }

        $lang = $request->getQueryParam('lang');
        $parameters = new ConceptSearchParameters($request, $this->model->getConfig(), true);
        $results = $this->model->searchConcepts($parameters);
        $hits = $this->findLookupHits($results, $label, $lang);
        $ret = $this->transformLookupResults($lang, $hits);
        if ($ret === null) {
            return $this->returnError(404, 'Not Found', "Could not find label '$label'");
        }
        return $this->returnJson($ret);
    }

    /**
     * Queries the top concepts of a vocabulary and wraps the results in a json-ld object.
     * @param Request $request
     * @return object json-ld object
     *
     *  @OA\Get(
     *     path="/{terminologyId}/topConcepts",
     *     @OA\Response(response="200", description="Terminology top concepts as JSON-LD"),
     *     @OA\Parameter(
     *         name="terminologyId",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="string")
     *     )
     *  )
     */
    public function topConcepts($request)
    {
        $vocab = $request->getVocab();
        if ($this->notModified($vocab)) {
            return null;
        }
        $scheme = $request->getQueryParam('scheme');
        if (!$scheme) {
            $scheme = $vocab->getConfig()->showConceptSchemesInHierarchy() ? array_keys($vocab->getConceptSchemes()) : $vocab->getDefaultConceptScheme();
        }

        /* encode the results in a JSON-LD compatible array */
        $topconcepts = $vocab->getTopConcepts($scheme, $request->getLang());

        // tisaf:uri
        $graphName = $request->getVocab()->getGraph();
        foreach ($topconcepts as &$topconcept) {
            $tisafUri = $this->getTisafUri($graphName, $topconcept["uri"]);
            if (!empty($tisafUri) && $tisafUri[0]->tisafUri instanceof Resource) {
                $topconcept["tisafUri"] = $tisafUri[0]->tisafUri->getUri();
            }
        }

        $ret = array_merge_recursive($this->context, array(
            '@context' => array('onki' => 'http://schema.onki.fi/onki#', 'topconcepts' => 'skos:hasTopConcept', 'notation' => 'skos:notation', 'label' => 'skos:prefLabel', '@language' => $request->getLang()),
            'uri' => $scheme,
            'topconcepts' => $topconcepts)
        );

        return $this->returnJson($ret);
    }

    private function redirectToVocabData($request) {
        $urls = $request->getVocab()->getConfig()->getDataURLs();
        if (sizeof($urls) == 0) {
            $vocid = $request->getVocab()->getId();
            return $this->returnError('404', 'Not Found', "No download source URL known for vocabulary $vocid");
        }

        $format = $this->negotiateFormat(array_keys($urls), $request->getServerConstant('HTTP_ACCEPT'), $request->getQueryParam('format'));
        if (!$format) {
            return $this->returnError(406, 'Not Acceptable', "Unsupported format. Supported MIME types are: " . implode(' ', array_keys($urls)));
        }
        if (is_array($urls[$format])) {
            $arr = $urls[$format];
            $dataLang = $request->getLang();
            if (isset($arr[$dataLang])) {
                header("Location: " . $arr[$dataLang]);
            } else {
                $vocid = $request->getVocab()->getId();
                return $this->returnError('404', 'Not Found', "No download source URL known for vocabulary $vocid in language $dataLang");
            }
		} else {
            header("Location: " . $urls[$format]);
		}
    }

    private function returnDataResults($results, $format, $compactification = true) {
        if ($compactification && ($format == 'application/ld+json' || $format == 'application/json')) {
            // further compact JSON-LD document using a context
            $context = array(
                'skos' => 'http://www.w3.org/2004/02/skos/core#',
                'isothes' => 'http://purl.org/iso25964/skos-thes#',
                'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
                'owl' => 'http://www.w3.org/2002/07/owl#',
                'dct' => 'http://purl.org/dc/terms/',
                'dc11' => 'http://purl.org/dc/elements/1.1/',
                'uri' => '@id',
                'type' => '@type',
                'lang' => '@language',
                'value' => '@value',
                'graph' => '@graph',
                'label' => 'rdfs:label',
                'prefLabel' => 'skos:prefLabel',
                'altLabel' => 'skos:altLabel',
                'hiddenLabel' => 'skos:hiddenLabel',
                'broader' => 'skos:broader',
                'narrower' => 'skos:narrower',
                'related' => 'skos:related',
                'inScheme' => 'skos:inScheme',
                'exactMatch' => 'skos:exactMatch',
                'closeMatch' => 'skos:closeMatch',
                'broadMatch' => 'skos:broadMatch',
                'narrowMatch' => 'skos:narrowMatch',
                'relatedMatch' => 'skos:relatedMatch',
            );
            $compactJsonLD = \ML\JsonLD\JsonLD::compact($results, json_encode($context));
            $results = \ML\JsonLD\JsonLD::toString($compactJsonLD);
        }

        header("Content-type: $format; charset=utf-8");
        echo $results;
    }

    /**
     * Download a concept as json-ld or redirect to download the whole vocabulary.
     * @param Request $request
     * @return object json-ld formatted concept.
     */
    public function data($request)
    {
        $vocab = $request->getVocab();
        if ($this->notModified($request->getVocab())) {
            return null;
        }

        if ($request->getUri()) {
            $uri = $request->getUri();
        } else if ($vocab !== null) { // whole vocabulary - redirect to download URL
            return $this->redirectToVocabData($request);
        } else {
            return $this->returnError(400, 'Bad Request', "uri parameter missing");
        }

        $format = $this->negotiateFormat(explode(' ', self::SUPPORTED_FORMATS), $request->getServerConstant('HTTP_ACCEPT'), $request->getQueryParam('format'));
        if (!$format) {
            return $this->returnError(406, 'Not Acceptable', "Unsupported format. Supported MIME types are: " . self::SUPPORTED_FORMATS);
        }

        $vocid = $vocab ? $vocab->getId() : null;
        // access concrete rdf data
        $results = $this->model->getRDF($vocid, $uri, $format);
        if (empty($results)) {
            return $this->returnError(404, 'Bad Request', "no concept found with given uri");
        }
        return $this->returnDataResults($results, $format);
    }

    /**
     * Download the terminology as RDF file
     *
     * @param Request $request
     *
     *  @OA\Get(
     *     path="/{terminologyId}/export",
     *     @OA\Response(response="200", description="Terminology as RDF file"),
     *     @OA\Parameter(
     *         name="terminologyId",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *          name="format",
     *          in="query",
     *          @OA\Schema(ref="#/components/schemas/format")
     *     ),
     *     @OA\Parameter(
     *          name="Accept",
     *          in="header",
     *          @OA\Schema(ref="#/components/schemas/format")
     *     )
     * )
     */
    public function dataTerminologyDownload(Request $request)
    {
        // TODO: potentially heavy load operation - optimization possible?
        $vocab = $request->getVocab();
        // '+' sign in format parameter must be encoded: application/ld+json, application/rdf+xml => application/rdf%2Bxml
        $format = $this->negotiateFormat(explode(' ', self::TISAF_SUPPORTED_FORMATS), $request->getServerConstant('HTTP_ACCEPT'), $request->getQueryParam('format'));
        if (!$format) {
            return $this->returnError(406, 'Not Acceptable', "Unsupported format. Supported MIME types are: " . self::SUPPORTED_FORMATS);
        }

        $gsEndpoint = $this->model->getConfig()->getGraphStoreEndpoint();
        $sparql = $this->model->getSparqlImplementation('JenaText', $gsEndpoint->getUri(), null);
        $uri = $sparql->client->getUri();
        $graphStore = new GraphStore($uri);
        $graph = $graphStore->get($vocab->getGraph());
        $terminologySerialization = $graph->serialise($format);

        if (empty($terminologySerialization)) {
            $vocabid = $vocab->getId();
            return $this->returnError(404, 'Bad Request', "Terminology serialization for '$vocabid' failed.");
        }
        return $this->returnDataResults($terminologySerialization, $format, false);
    }

    /**
     * Get the mappings associated with a concept, enriched with labels and notations.
     * Returns a JSKOS-compatible JSON object.
     * @param Request $request
     * @throws Exception if the vocabulary ID is not found in configuration
     */
    public function mappings(Request $request)
    {
        $this->setLanguageProperties($request->getLang());
        $vocab = $request->getVocab();
        if ($this->notModified($vocab)) {
            return null;
        }

        $uri = $request->getUri();
        if (!$uri) {
            return $this->returnError(400, 'Bad Request', "uri parameter missing");
        }

        $queryExVocabs = $request->getQueryParamBoolean('external', true);

        $results = $vocab->getConceptInfo($uri, $request->getContentLang());
        if (empty($results)) {
            return $this->returnError(404, 'Bad Request', "no concept found with given uri");
        }

        $concept = $results[0];

        $mappings = [];
        foreach ($concept->getMappingProperties() as $mappingProperty) {
            foreach ($mappingProperty->getValues() as $mappingPropertyValue) {
                $hrefLink = $this->linkUrlFilter($mappingPropertyValue->getUri(), $mappingPropertyValue->getExVocab(), $request->getLang(), 'page', $request->getContentLang());
                $mappings[] = $mappingPropertyValue->asJskos($queryExVocabs, $request->getLang(), $hrefLink);
            }
        }

        $ret = array(
            'mappings' => $mappings,
            'graph' => $concept->dumpJsonLd()
        );

        return $this->returnJson($ret);
    }

    /**
     * Used for querying labels for a uri.
     * @param Request $request
     * @return object json-ld wrapped labels.
     */
    public function label($request)
    {
        if (!$request->getUri()) {
            return $this->returnError(400, "Bad Request", "uri parameter missing");
        }

        if ($this->notModified($request->getVocab())) {
            return null;
        }

        $vocab = $request->getVocab();
        if ($vocab === null) {
            $vocab = $this->model->guessVocabularyFromUri($request->getUri());
        }
        if ($vocab === null) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }

        $labelResults = $vocab->getAllConceptLabels($request->getUri(), $request->getLang());
        if ($labelResults === null) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }

        // there should be only one preferred label so no need for an array
        if (array_key_exists('prefLabel', $labelResults)) {
            $labelResults['prefLabel'] = $labelResults['prefLabel'][0];
        }

        $ret = array_merge_recursive($this->context,
                                    array('@context' => array('prefLabel' => 'skos:prefLabel', 'altLabel' => 'skos:altLabel', 'hiddenLabel' => 'skos:hiddenLabel', '@language' => $request->getLang()),
                                    'uri' => $request->getUri()),
                                    $labelResults);

        return $this->returnJson($ret);
    }

    /**
     * Query for the available letters in the alphabetical index.
     * @param Request $request
     * @return object JSON-LD wrapped list of letters
     */

    public function indexLetters($request)
    {
        $this->setLanguageProperties($request->getLang());
        $letters = $request->getVocab()->getAlphabet($request->getLang());

        $ret = array_merge_recursive($this->context, array(
            '@context' => array(
                'indexLetters' => array(
                    '@id' => 'skosmos:indexLetters',
                    '@container' => '@list',
                    '@language' => $request->getLang()
                )
            ),
            'uri' => '',
            'indexLetters' => $letters)
        );
        return $this->returnJson($ret);
    }

    /**
     * Query for the concepts with terms starting with a given letter in the
     * alphabetical index.
     * @param Request $request
     * @return object JSON-LD wrapped list of terms/concepts
     */

    public function indexConcepts($letter, $request)
    {
        $this->setLanguageProperties($request->getLang());

        $offset_param = $request->getQueryParam('offset');
        $offset = (is_numeric($offset_param) && $offset_param >= 0) ? $offset_param : 0;
        $limit_param = $request->getQueryParam('limit');
        $limit = (is_numeric($limit_param) && $limit_param >= 0) ? $limit_param : 0;

        $concepts = $request->getVocab()->searchConceptsAlphabetical($letter, $limit, $offset, $request->getLang());

        $ret = array_merge_recursive($this->context, array(
            '@context' => array(
                'indexConcepts' => array(
                    '@id' => 'skosmos:indexConcepts',
                    '@container' => '@list'
                )
            ),
            'uri' => '',
            'indexConcepts' => $concepts)
        );
        return $this->returnJson($ret);
    }

    private function transformPropertyResults($uri, $lang, $objects, $propname, $propuri)
    {
        $results = array();
        foreach ($objects as $objuri => $vals) {
            $objuri = !empty($vals["tisaf:uri"]) ? $vals["tisaf:uri"] : $objuri;
            $results[] = array('uri' => $objuri, 'prefLabel' => $vals['label']);
        }

        return array_merge_recursive($this->context, array(
            '@context' => array('prefLabel' => 'skos:prefLabel', $propname => $propuri, '@language' => $lang),
            'uri' => $uri,
            $propname => $results)
        );
    }

    private function transformTransitivePropertyResults($uri, $lang, $objects, $tpropname, $tpropuri, $dpropname, $dpropuri)
    {
        $results = array();
        foreach ($objects as $objuri => $vals) {
            $result = array('uri' => $objuri, 'prefLabel' => $vals['label']);
            if (isset($vals['direct'])) {
                $result[$dpropname] = $vals['direct'];
            }
            $results[$objuri] = $result;
        }

        return array_merge_recursive($this->context, array(
            '@context' => array('prefLabel' => 'skos:prefLabel', $dpropname => array('@id' => $dpropuri, '@type' => '@id'), $tpropname => array('@id' => $tpropuri, '@container' => '@index'), '@language' => $lang),
            'uri' => $uri,
            $tpropname => $results)
        );
    }

    /**
     * Used for querying broader relations for a concept.
     * @param Request $request
     * @return object json-ld wrapped broader concept uris and labels.
     */
    public function broader($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $broaders = $request->getVocab()->getConceptBroaders($request->getUri(), $request->getLang());
        if ($broaders === null) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }
        $ret = $this->transformPropertyResults($request->getUri(), $request->getLang(), $broaders, "broader", "skos:broader");
        return $this->returnJson($ret);
    }

    /**
     * Used for querying broader transitive relations for a concept.
     * @param Request $request
     * @return object json-ld wrapped broader transitive concept uris and labels.
     */
    public function broaderTransitive($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $broaders = $request->getVocab()->getConceptTransitiveBroaders($request->getUri(), $this->parseLimit(), false, $request->getLang());
        if (empty($broaders)) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }
        $ret = $this->transformTransitivePropertyResults($request->getUri(), $request->getLang(), $broaders, "broaderTransitive", "skos:broaderTransitive", "broader", "skos:broader");
        return $this->returnJson($ret);
    }

    /**
     * Used for querying narrower relations for a concept.
     * @param Request $request
     * @return object json-ld wrapped narrower concept uris and labels.
     */
    public function narrower($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $narrowers = $request->getVocab()->getConceptNarrowers($request->getUri(), $request->getLang());

        // if uri owl:sameAs object -> get info from object too in result!
        $uri = $request->getUri();
        $vocab = $request->getVocab();

        $concepts = $vocab->getConceptInfo($uri, $request->getContentLang());
        $narrowersTwin = [];
        if (!empty($concepts) && $concepts[0] && $concepts[0]->getSameAs() instanceof Resource) {
            $tisafUriValue = $concepts[0]->getSameAs()->getUri();
            $tisaf = $this->model->getConfig()->getTisafPrefix();
            $tisafUri = $tisaf . "uri";
            // search for concept with ?s tisaf:uri tisafUriValue
            foreach ($this->graphs(false) as $graphName) {
                $queryString = <<<EOD
SELECT DISTINCT ?s
FROM <$graphName>
WHERE{
    ?s <$tisafUri> <$tisafUriValue>
}
EOD;
                $result = $request->getVocab()->getSparql()->execute($queryString);
                if ($result instanceof Result && $result->count() == 1) {
                    // TODO: additional check: check if graphName -> vocabId -> tisafUriValue.contains(vocabId)
                    $twinUri = $result[0]->s->getUri();
                    $twinVocab = $this->model->getVocabularyByGraph($graphName);
                    $narrowersTwin = $twinVocab->getConceptNarrowers($twinUri, $request->getLang());
                    break;
                }
            }
        }

        $narrowers = self::addTisafUriForConcepts($vocab, $narrowers, $request->getLang());
        $narrowersTwin = self::addTisafUriForConcepts($twinVocab, $narrowersTwin, $request->getLang());

        // add two narrowers arrays
        $narrowers = $narrowers + $narrowersTwin;

        if ($narrowers === null) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }
        $ret = $this->transformPropertyResults($request->getUri(), $request->getLang(), $narrowers, "narrower", "skos:narrower");

        // add 2 "parents" uris
        $conceptTisafUri = self::getConceptTisafUri($vocab, $request->getURI(), $request->getLang());
        $parentUris[] = ["uri" => $conceptTisafUri];
        if(!empty($twinUri)) {
            $parentUris[] = ["uri" => self::getConceptTisafUri($twinVocab, $request->getURI(), $request->getLang())];
        }
        $ret["parentUris"] = $parentUris;
        $ret["uri"] = $conceptTisafUri;
        return $this->returnJson($ret);
    }

    private static function getConceptTisafUri(Vocabulary $voc, $uri, $lang) {
        $concepts = $voc->getConceptInfo($uri, $lang);
        if (sizeof($concepts) == 1) {
            $concept = $concepts[0];

            // tisaf:uri, if existing (see tisafUriMigration)
            $tisafUri = $concept->getTisafUri();
            return $tisafUri;
        }
        return $uri;
    }

    /**
     * Used for querying narrower transitive relations for a concept.
     * @param Request $request
     * @return object json-ld wrapped narrower transitive concept uris and labels.
     */
    public function narrowerTransitive($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $narrowers = $request->getVocab()->getConceptTransitiveNarrowers($request->getUri(), $this->parseLimit(), $request->getLang());
        if (empty($narrowers)) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }
        $ret = $this->transformTransitivePropertyResults($request->getUri(), $request->getLang(), $narrowers, "narrowerTransitive", "skos:narrowerTransitive", "narrower", "skos:narrower");
        return $this->returnJson($ret);
    }

    /**
     * Used for querying broader transitive relations
     * and some narrowers for a concept in the hierarchy view.
     * @param Request $request
     * @return object json-ld wrapped hierarchical concept uris and labels.
     */
    public function hierarchy($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $results = $request->getVocab()->getConceptHierarchy($request->getUri(), $request->getLang());
        if (empty($results)) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }

        // set the "top" key from the "tops" key
        foreach ($results as $value) {
            $uri = $value['uri'];
            if (isset($value['tops'])) {
                if ($request->getVocab()->getConfig()->getMainConceptSchemeURI() != null) {
                    foreach ($results[$uri]['tops'] as $top) {
                        // if a value in 'tops' matches the main concept scheme of the vocabulary, take it
                        if ($top == $request->getVocab()->getConfig()->getMainConceptSchemeURI()) {
                            $results[$uri]['top'] = $top;
                            break;
                        }
                    }
                    // if the main concept scheme was not found, set 'top' to the first 'tops' (sorted alphabetically on the URIs)
                    if (! isset($results[$uri]['top'])) {
                        $results[$uri]['top'] = $results[$uri]['tops'][0];
                    }
                } else {
                    // no main concept scheme set on the vocab, take the first value of 'tops' (sorted alphabetically)
                    $results[$uri]['top'] = $results[$uri]['tops'][0];
                }
            }
        }

        if ($request->getVocab()->getConfig()->getShowHierarchy()) {
            $schemes = $request->getVocab()->getConceptSchemes($request->getLang());
            foreach ($schemes as $scheme) {
                if (!isset($scheme['title']) && !isset($scheme['label']) && !isset($scheme['prefLabel'])) {
                    unset($schemes[array_search($scheme, $schemes)]);
                }

            }

            /* encode the results in a JSON-LD compatible array */
            $topconcepts = $request->getVocab()->getTopConcepts(array_keys($schemes), $request->getLang());
            foreach ($topconcepts as $top) {
                if (!isset($results[$top['uri']])) {
                    $results[$top['uri']] = array('uri' => $top['uri'], 'top'=>$top['topConceptOf'], 'tops'=>array($top['topConceptOf']), 'prefLabel' => $top['label'], 'hasChildren' => $top['hasChildren']);
                    if (isset($top['notation'])) {
                        $results[$top['uri']]['notation'] = $top['notation'];
                    }

                }
            }
        }

        $ret = array_merge_recursive($this->context, array(
            '@context' => array(
                'onki' => 'http://schema.onki.fi/onki#',
                'prefLabel' => 'skos:prefLabel',
                'notation' => 'skos:notation',
                'narrower' => array('@id' => 'skos:narrower', '@type' => '@id'),
                'broader' => array('@id' => 'skos:broader', '@type' => '@id'),
                'broaderTransitive' => array('@id' => 'skos:broaderTransitive', '@container' => '@index'),
                'top' => array('@id' => 'skos:topConceptOf', '@type' => '@id'),
                // the tops key will contain all the concept scheme values, while top (singular) contains a single value
                'tops' => array('@id' => 'skos:topConceptOf', '@type' => '@id'),
                'hasChildren' => 'onki:hasChildren',
                '@language' => $request->getLang()
            ),
            'uri' => $request->getUri(),
            'broaderTransitive' => $results)
        );

        return $this->returnJson($ret);
    }

    /**
     * Used for querying group hierarchy for the sidebar group view.
     * @param Request $request
     * @return object json-ld wrapped hierarchical concept uris and labels.
     */
    public function groups($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $results = $request->getVocab()->listConceptGroups($request->getLang());

        $ret = array_merge_recursive($this->context, array(
            '@context' => array('onki' => 'http://schema.onki.fi/onki#', 'prefLabel' => 'skos:prefLabel', 'groups' => 'onki:hasGroup', 'childGroups' => array('@id' => 'skos:member', '@type' => '@id'), 'hasMembers' => 'onki:hasMembers', '@language' => $request->getLang()),
            'uri' => '',
            'groups' => $results)
        );

        return $this->returnJson($ret);
    }

    /**
     * Used for querying member relations for a group.
     * @param Request $request
     * @return object json-ld wrapped narrower concept uris and labels.
     */
    public function groupMembers($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $children = $request->getVocab()->listConceptGroupContents($request->getUri(), $request->getLang());
        if (empty($children)) {
            return $this->returnError('404', 'Not Found', "Could not find group <{$request->getUri()}>");
        }

        $ret = array_merge_recursive($this->context, array(
            '@context' => array('prefLabel' => 'skos:prefLabel', 'members' => 'skos:member', '@language' => $request->getLang()),
            'uri' => $request->getUri(),
            'members' => $children)
        );

        return $this->returnJson($ret);
    }

    /**
     * Used for querying narrower relations for a concept in the hierarchy view.
     * @param Request $request
     * @return object json-ld wrapped narrower concept uris and labels.
     */
    public function children($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $children = $request->getVocab()->getConceptChildren($request->getUri(), $request->getLang());
        if ($children === null) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }

        $ret = array_merge_recursive($this->context, array(
            '@context' => array('prefLabel' => 'skos:prefLabel', 'narrower' => 'skos:narrower', 'notation' => 'skos:notation', 'hasChildren' => 'onki:hasChildren', '@language' => $request->getLang()),
            'uri' => $request->getUri(),
            'narrower' => $children)
        );

        return $this->returnJson($ret);
    }

    /**
     * Used for querying narrower relations for a concept in the hierarchy view.
     * @param Request $request
     * @return object json-ld wrapped hierarchical concept uris and labels.
     */
    public function related($request)
    {
        if ($this->notModified($request->getVocab())) {
            return null;
        }
        $related = $request->getVocab()->getConceptRelateds($request->getUri(), $request->getLang());
        if ($related === null) {
            return $this->returnError('404', 'Not Found', "Could not find concept <{$request->getUri()}>");
        }
        $ret = $this->transformPropertyResults($request->getUri(), $request->getLang(), $related, "related", "skos:related");
        return $this->returnJson($ret);
    }

    /**
     * Used for querying new concepts in the vocabulary
     * @param Request $request
     * @return object json-ld wrapped list of changed concepts
     */
    public function newConcepts($request)
    {
        $offset = ($request->getQueryParam('offset') && is_numeric($request->getQueryParam('offset')) && $request->getQueryParam('offset') >= 0) ? $request->getQueryParam('offset') : 0;
        $limit = ($request->getQueryParam('limit') && is_numeric($request->getQueryParam('limit')) && $request->getQueryParam('limit') >= 0) ? $request->getQueryParam('limit') : 200;

        return $this->changedConcepts($request, 'dc:created', $offset, $limit);
    }

    /**
     * Used for querying modified concepts in the vocabulary
     * @param Request $request
     * @return object json-ld wrapped list of changed concepts
     */
    public function modifiedConcepts($request)
    {
        $offset = ($request->getQueryParam('offset') && is_numeric($request->getQueryParam('offset')) && $request->getQueryParam('offset') >= 0) ? $request->getQueryParam('offset') : 0;
        $limit = ($request->getQueryParam('limit') && is_numeric($request->getQueryParam('limit')) && $request->getQueryParam('limit') >= 0) ? $request->getQueryParam('limit') : 200;

        return $this->changedConcepts($request, 'dc:modified', $offset, $limit);
    }

    /**
     * Used for querying changed concepts in the vocabulary
     * @param Request $request
     * @param int $offset starting index offset
     * @param int $limit maximum number of concepts to return
     * @return object json-ld wrapped list of changed concepts
     */
    private function changedConcepts($request, $prop, $offset, $limit)
    {
        $changeList = $request->getVocab()->getChangeList($prop, $request->getLang(), $offset, $limit);

        $simpleChangeList = array();
        foreach($changeList as $conceptInfo) {
            if (array_key_exists('date', $conceptInfo)) {
                $simpleChangeList[] =  array( 'uri' => $conceptInfo['uri'],
                                               'prefLabel' => $conceptInfo['prefLabel'],
                                               'date' => $conceptInfo['date']->format("Y-m-d\TH:i:sO") );
            }
        }
        return $this->returnJson(array_merge_recursive($this->context,
                                                        array('@context' => array( '@language' => $request->getLang(),
                                                                                     'prefLabel' => 'skos:prefLabel',
                                                                                     'xsd' => 'http://www.w3.org/2001/XMLSchema#',
                                                                                     'date' => array( '@id' => 'http://purl.org/dc/terms/date', '@type' => 'http://www.w3.org/2001/XMLSchema#dateTime') )
                                                        ),
                                                        array('changeList' => $simpleChangeList)));

    }

    /**
     * Imports a new terminology including meta data and tags.
     *
     * @OA\Post(
     *     path="/import",
     *     summary="Import a new terminology with form data",
     *     @OA\Response(
     *         response=201,
     *         description="Terminology imported"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input"
     *     ),
     *     @OA\RequestBody(
     *         description="Terminology Form Data upload",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
                        @OA\Property(
     *                     property="terminologyID",
     *                     description="Terminology shortname (identifier)",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="source",
     *                     description="Terminology source (URL or other unique identifier)",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="title",
     *                     description="Terminology title",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="authors",
     *                     description="Comma separated list of author names",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="selectedTags",
     *                     description="Comma separated list of TISAF terminology tags",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="notes",
     *                     description="Terminology description/notes",
     *                     type="string",
     *                 ),
     *                @OA\Property(
     *                     property="version",
     *                     description="Terminology version",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="skosFile",
     *                     description="Terminology SKOS RDF File",
     *                     format="binary",
     *                 ),
     *             )
     *         )
     *     )
     *  )
     */
    public function import()
    {
        // This is the entire file that was uploaded to a temp location.
        $skosFile = $_FILES['skosFile'];
        if ($skosFile["error"] === 1) {
            return $this->returnError(400, "Bad Request", "Input file was not uploaded correctly. Please check the name, size and file type (e.g. application/rdf+xml, text/turtle, application/ld+json, application/json) of the given file.");
        }
        $localFile = $skosFile['tmp_name'];
        $graph = new EasyRdf\Graph();
        try {
            $newTriples = $graph->parseFile($localFile, $skosFile['type'], $_POST['source']);
        } catch (Exception $e) {
            header("HTTP/1.0 400 Bad Request");
            $result = ["devError" => $e->getMessage(), "userError" => "Error while parsing input RDF file"];
	   error_log(json_encode($result));
	    echo json_encode($result);
            return;
        }

        $newVocabId = $_POST['terminologyID'];
        $vocabIDs = $this->vocabIDs(false);
        if (sizeof($vocabIDs) > 0 && in_array($newVocabId, $vocabIDs)) {
            header("HTTP/1.0 400 Bad Request");
            echo ("Error: invalid argument - terminology id '". $newVocabId ."' already in use!");
            return;
        }

        $newGraphName = $_POST['source'];
        $graphNames = self::graphs(false);
        if (sizeof($graphNames) > 0 && in_array($newGraphName, $graphNames)) {
            header("HTTP/1.0 400 Bad Request");
            echo ("Error: invalid argument - graph name '". $newGraphName ."' already in use!");
            return;
        }
        $title = $_POST['title'];
        $authors = array_filter(explode(',', $_POST['authors']));
        $tags = array_filter(explode(',', $_POST['selectedTags']));
        $notes = $_POST['notes'];
        $version = $_POST['version'];

        // check that concept scheme count == 1
        $conceptSchemes = $graph->allOfType("skos:ConceptScheme");
        if (count($conceptSchemes) !== 1) {
            header("HTTP/1.0 400 Bad Request");
            echo ("Error: expected input terminology to have exact 1 skos:ConceptScheme, found: " . count($conceptSchemes));
            return;
        }

        // add meta data (with tisaf prefix)
        $cs = $conceptSchemes[0];
        $metaDataResult = self::addTisafMetaDataToConceptScheme($cs, $title, $authors, $newGraphName, $version, $notes);

        if ($metaDataResult['success'] !== true) {
            header("HTTP/1.0 500 Server Error");
            echo ("Import failed on adding meta data.");
            return;
        }

        // add tags
        $tagsResult = self::addTagsToConceptScheme($cs, $tags);
        if ($tagsResult !== true) {
            header("HTTP/1.0 500 Server Error");
            echo ("Import failed on adding tags.");
            return;
        }

        $uriRef = $metaDataResult['graphName'];
        if (empty($uriRef)) {
            header("HTTP/1.0 500 Server Error");
            echo ("Import failed - invalid uri ref (to be used graph name).");
            return;
        }
        if (in_array($uriRef, self::graphs(false))) {
            header("HTTP/1.0 400 Bad Request");
            echo ("Error: invalid argument - graph name '". $uriRef ."' already in use!");
            return;
        }

        // graph store
        $gsEndpoint = $this->model->getConfig()->getGraphStoreEndpoint();
        $sparql = $this->model->getSparqlImplementation('JenaText', $gsEndpoint->getUri(), null);
        $uri = $sparql->client->getUri();
        $gs = new GraphStore($uri);

        // add graph!
        try {
            $format = EasyRdf\Format::getFormat($skosFile['type']);
            $gs->replace($graph, $uriRef, $format->getName());
        } catch (Exception $e) {
            header("HTTP/1.0 500 Server Error");
            echo ($e);
        }

        // update config.ttl -> finally add terminology to whole system
        self::addTerminologyToConfig($newVocabId, $title, $uriRef);

        // execute tisaf:uri migration, too
        $success = TisafUriHelper::setTisafUrisForTerminology($newVocabId);
        if (!$success) {
            error_log("tisaf:uri migration for terminology '$newVocabId' NOT successful !!!");
        }

        // return new URI after successful operation
        header("HTTP/1.0 201 Created");
        $terminologyUrl = $this->model->getConfig()->getFullSystemHost() . $newVocabId;
        header("Location: $terminologyUrl");
    }

    private function addTerminologyToConfig($terminologyID, $title, $graph) {
        $sparqlEndpoint = $this->model->getConfig()->getDefaultEndpoint();
        $configEntryString = <<<EOD

:$terminologyID a skosmos:Vocabulary, void:Dataset ;
    dc:title "$title"@en ;
    skosmos:shortName "$terminologyID";
    dc:subject :cat_general ;
    void:uriSpace "$graph";
    skosmos:language "en";
    skosmos:defaultLanguage "en";
    void:sparqlEndpoint <$sparqlEndpoint> ;
    skosmos:sparqlGraph <$graph> .

EOD;
        file_put_contents($this->model->getConfig()->filePath, $configEntryString, FILE_APPEND | LOCK_EX);
    }


    private function validate_url($url) {
        $path = parse_url($url, PHP_URL_PATH);
        $encoded_path = array_map('urlencode', explode('/', $path));
        $url = str_replace($path, implode('/', $encoded_path), $url);

        return filter_var($url, FILTER_VALIDATE_URL);
    }

    private function addTisafMetaDataToConceptScheme(Resource $conceptScheme, $title, $authors, $newGraphName, $version, $notes) {
        // tisaf prefix workaround:
        // workaround since new terminology wont have the 'tisaf' or 'tt' prefix in its header part (e.g. tisaf:terminologyTitle)

        // tisaf:importDate
        $xsdImportDate = date("Y-m-d");
        $conceptScheme->addLiteral($this->model->getConfig()->getTisafPrefix() . 'importDate', $xsdImportDate);

        // tisaf:terminologyTitle
        $conceptScheme->addLiteral($this->model->getConfig()->getTisafPrefix() . 'terminologyTitle', $title, "en");

        // authors
        //tisaf:terminologyAuthors
        $conceptScheme->addLiteral($this->model->getConfig()->getTisafPrefix() . 'terminologyAuthors', $authors, "en");

        // graph name / tisaf:source
        $url = filter_var($newGraphName, FILTER_SANITIZE_URL);
        $graphName = null;
        if (isset($url)) {
            if (in_array($url, self::graphs(false))) {
                // graph name already used
                return $this->returnError(400, "Bad Request", "Invalid source/graph name parameter: " . $newGraphName);
            }
            if (self::validate_url($url)) {
                // set new graph name
                $graphName = $url;
                $conceptScheme->addLiteral($this->model->getConfig()->getTisafPrefix() . 'source', $url, "en");
            } else {
                // not a valid url, but custom identifier, e.g. rdflicense-12312149
                $customIdentifier = $url;
                $graphName = $this->model->getConfig()->getFullSystemHost() . "data/" . $customIdentifier;
            }
        }

        // version number - owl:versionInfo - optional param
        if (isset($version) && !empty($version)) {
            if (!preg_match("/((?:[0-9]+\.?)+)/i", $version)) {
              return $this->returnError(400, "Bad Request", "Invalid version parameter: " . $version);
            }
            $conceptScheme->addLiteral($this->model->getConfig()->getTisafPrefix() . 'versionInfo', $version, "en");
        }

        // notes - skos:note - optional param
        if (isset($notes) && !empty($notes)) {
            $notesSanitized = filter_var($notes, FILTER_SANITIZE_STRING);
            if (!empty($notesSanitized)) {
                $skosPrefix = RdfNamespace::get('skos');
                $conceptScheme->addLiteral($skosPrefix . 'note', $notesSanitized, "en");
            }
        }

        return ['success' => $graphName !== null, 'graphName' => $graphName];
    }

    private function addTagsToConceptScheme(Resource $conceptScheme, $tags) {
        $tagsFiltered = array_filter($tags, function ($v, $k) {
            $tisafTTPrefix = $this->model->getConfig()->getTisafTagsPrefix();
            return is_numeric($k) && str_starts_with($v, $tisafTTPrefix);
        }, ARRAY_FILTER_USE_BOTH);
        // prop: tisaf:hasTags, value: link(s) for tag(s)
        $prop = $this->model->getConfig()->getTisafPrefix() . "hasTags";
        foreach ($tagsFiltered as $tagUri) {
            $conceptScheme->addResource($prop, $tagUri);
        }
        return true;
    }

    public function graphs(bool $encode = true)
    {
        $result = [];
        $sparql = $this->model->getDefaultSparql();
        if ($sparql instanceof GenericSparql) {
            $namedGraphs = $sparql->client->listNamedGraphs();
            foreach ($namedGraphs as $graph) {
                $result[] = $graph->getUri();
            }
        }
        if ($encode) {
            header('Content-type: application/json');
            echo json_encode($result);
        } else {
            return $result;
        }
    }

    // TODO: is that endpoint really needed? BETTER: /terminologies -> meta data including id etc.!!!!!!!
    public function vocabIDs(bool $encode = true)
    {
        $vocabs = $this->model->getVocabularyList(false);
        $result = [];
        foreach ($vocabs as $vocab) {
            $result[] = $vocab->getId();
        }
        if ($encode) {
            header('Content-type: application/json');
            echo json_encode($result);
        } else {
            return $result;
        }
    }

    /**
     * @OA\Post(
     *     path="/misstermeval2",
     *     summary="Add a new skos:Concept to a defined terminology",
     *     @OA\RequestBody(
     *      @OA\JsonContent(
     *          type="object",
     *          @OA\Property(
     *              property="attribute",
     *              description="Attribute name, e.g. 'research-area'",
     *              example="research-area",
     *              type="string",
     *          ),
     *          @OA\Property(
     *              property="value",
     *              description="User input value",
     *              example="transmission electron microscope",
     *              type="string",
     *          ),
     *          @OA\Property(
     *              property="reason",
     *              description="Reason why value was added, e.g. no satisfying match for input",
     *              example="No results found for input",
     *              type="string",
     *          ),
     *      )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Concept created"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid input"
     *     )
     * )
     */
    public function addMissingTermFromEvalTwo()
    {
        // load graph by name -> get name from config
        $graphName = $this->model->getConfig()->getTisafMissingTermsTwoGraph();

        $data = json_decode(file_get_contents('php://input'), true);

        // fallback if data is send via normal ajax request
        if (empty($data)) {
         $data = $_POST;
        }

        $attribute = filter_var($data["attribute"], FILTER_SANITIZE_STRING);
        $value = filter_var($data["value"], FILTER_SANITIZE_STRING);
        $reason = filter_var($data["reason"], FILTER_SANITIZE_STRING);

        if (empty($value)) {
            return $this->returnError(400, "Bad Request", "Invalid value parameter");
        }

        $vocab = $this->model->getVocabularyByGraph($graphName);

        // concept count by query
	// graphName == http://tisaf.de/missing/terms/graph/
        $countQueryString = <<<EOD
      SELECT (count(distinct ?s) as ?count)
FROM <$graphName>
WHERE {
        ?s a skos:Concept;
}
EOD;
        $sparql = $vocab->getSparql();
        $countResult = $sparql->execute($countQueryString);

        // count skos:Concept
        $conceptCount = rand();
 	    try {
                $conceptCount = $countResult[0]->count->getValue();
        } catch (Exception $e) {
                error_log("Exception" . $e->getMessage());
        }

        $fullhost = $this->model->getConfig()->getTisafMissingTermsTwoGraph();

        $creationDate = date("Y-m-d");
        // create skos:Concept
        $queries = [];
        $id = $conceptCount + 1;
        $queries[] = "<$fullhost/$id> a skos:Concept";
        $queries[] = "<$fullhost/$id> skos:prefLabel \"$value\"@en";
        $queries[] = "<$fullhost/$id> skos:altLabel \"$attribute\"@en";
        $queries[] = "<$fullhost/$id> skos:note \"$reason\"@en";
        $queries[] = "<$fullhost/$id> dc:date \"$creationDate\"";
        $queries[] = "<$fullhost/$id> skos:topConceptOf <$fullhost/root>";

        // insert new triples!
        foreach ($queries as $query) {
            $sparql->insert($query, $graphName);
        }

        // 201 success status
        header("HTTP/1.0 201 Created");
        header("Location: $fullhost/$id");
    }
}
