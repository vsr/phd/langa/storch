<?php

use EasyRdf\Literal;
use EasyRdf\RdfNamespace;
use EasyRdf\Resource;
use EasyRdf\Sparql\Result;

require_once 'vendor/autoload.php';
// enable CORS for the whole REST API
header("Access-Control-Allow-Origin: *");

/**
 * this is a migration script to add rdf triples with a newly created tisaf-URI,
 * based on the *ENGLISH* label (skos:prefLabel, ...) for each concept of the vocabulary
 * specified by $vocabId (see below)
 */

class TisafUriHelper {
    public static function setTisafUrisForTerminology(string $terminologyId): bool
    {
        $config = new GlobalConfig();
        $model = new Model($config);
        $request = new Request($model);
        $request->setVocab($terminologyId);
        $sparql = $request->getVocab()->getSparql();
        $graphName = $request->getVocab()->getGraph();
        $tisafHost = $config->getFullSystemHost();

        // count of DISTINCT skos:Concept/skos:ConceptScheme
        $queryString = <<<EOD
SELECT (COUNT(?s) AS ?count)
FROM <$graphName>
WHERE {?s a skos:Concept}
EOD;
        $conceptCount = TisafUriHelper::getCountForQuery($sparql, $queryString);
        $queryString = <<<EOD
SELECT (COUNT(?s) AS ?count)
FROM <$graphName>
WHERE {?s a skos:ConceptScheme}
EOD;
        $schemeCount = TisafUriHelper::getCountForQuery($sparql, $queryString);

        // query resources with skos:prefLabel defined!
        // TODO: what about concepts where skos:prefLabel is not set? -> look for other properties (fallback)?
        // manual label@en prioritisation:
        $queryString = <<<EOD
SELECT DISTINCT ?s ?type ?label
FROM <$graphName>
WHERE {
  ?s a ?type .
  FILTER(?type = skos:Concept || ?type = skos:ConceptScheme) .
  {  ?s skos:prefLabel ?label .
     FILTER (lang(?label) = 'en')
  } UNION {
    ?s skos:prefLabel ?label .
     FILTER (lang(?label) != 'en')
  }
}
EOD;

        // exec. query (read)
        $result = $sparql->execute($queryString);
        $origCount = ($result instanceof Result) ? $result->numRows() : 0;

        if ($origCount === 0) {
            error_log("Result row count = 0, no tisaf uris will be created.");
            return false;
        }

        // TODO: add check if all labels are usable & unique
        $tisafUriProperty = $config->getTisafUriProperty();
        $result = TisafUriHelper::createTisafUriQueries($result, $terminologyId, $tisafUriProperty, $tisafHost);
        $queries = $result["queries"];

        // exec. update
        // warning - potentially heavy load operation (e.g. on a vocabulary with 5k+ concepts...)
        foreach ($queries as $query) {
            $sparql->insert($query, $graphName);
        }

        // info
        $conceptsCounter = $result["conceptsCounter"];
        // only 1 ConceptScheme should be in 1 Vocab -> so only 1 tisaf uri created in that vocabulary too
        $schemesCounter = $result["schemesCounter"];
        // also check if the expected count equals the skos:Concept + skos:ConceptSchemes count
        $expectedCount = $result["expectedCount"];
        $success = ($schemesCounter === 1) && ($conceptsCounter + $schemesCounter) === $expectedCount;
        if (!$success) {
            error_log("tisafUriMigration: expectedCount: $expectedCount, conceptsCount: $conceptsCounter, schemesCount: $schemesCounter");
        }

        $tisafUriProperty = $model->getConfig()->getTisafPrefix() . "uri";
        $tisafConceptUrisCreated = TisafUriHelper::getTisafUrisCreatedCount("skos:Concept", $tisafUriProperty, $graphName, $sparql);
        $tisafSchemesUrisCreated = TisafUriHelper::getTisafUrisCreatedCount("skos:ConceptScheme", $tisafUriProperty, $graphName, $sparql);
        // analysis how many uris where created:
        echo "skos:Concepts: \n\r $conceptsCounter with skos:prefLabel, $tisafConceptUrisCreated tisaf:uri's created,  $conceptCount overall\n\r";
        echo "skos:ConceptScheme: \n\r $schemesCounter with skos:prefLabel, $tisafSchemesUrisCreated tisaf:uri's created, $schemeCount overall\n\r";
        return $success;
    }

    private static function getTisafUrisCreatedCount($type, $tisafUriProperty, $graphName, $sparql) {
        $queryString = <<<EOD
SELECT (COUNT(?s) AS ?count)
FROM <$graphName>
WHERE {
    ?s a $type .
    ?s <$tisafUriProperty> ?uri
}
EOD;
        $result = $sparql->execute($queryString);
        $resultCount = -1;
        if (!empty($result) && $result[0] && $result[0]->count instanceof Literal\Integer) {
            $resultCount = $result[0]->count->getValue();
        }
        return $resultCount;
    }

    private static function createTisafUriQueries($result, string $vocabId, string $tisafUriProperty, string $tisafHost) {
        $queries = array();
        $schemesCounter = 0;
        $conceptsCounter = 0;
        $uniqueSubjects = [];
        foreach ($result as $triple) {
            if ($triple->label instanceof Literal) {
                $origLabel = trim($triple->label->getValue());
                $subjectUri = $triple->s->getUri();

                // lang = null or empty
                if (empty($triple->label->getLang())) {
                    error_log("Unusable language for skos:prefLabel of: " . $subjectUri);
                }

                $origLabel = self::transformUmlaute($origLabel);

                // check if all characters are valid ASCII
                // only 1 tisaf:uri query needed and wanted.
                if (!key_exists($subjectUri, $queries)) {
                    $tisafConceptIDPart = preg_replace('/\s+/', '_', $origLabel);
                    // special case: label contains "/" (slash), should not be encoded, else we get problems with url
                    // redirecting, because the web server & php explode processes this as an delimiter or sub-path/-dir of the URL!
                    $tisafConceptIDPart = str_replace("/", "_", $tisafConceptIDPart);
                    if ($triple->type && $triple->type instanceof Resource) {
                        $type = $triple->type->getUri();
                        if ($type === RdfNamespace::get("skos") . "Concept") {
                            $newURI = $tisafHost . $vocabId . '/' . urlencode($tisafConceptIDPart);
                            $conceptsCounter++;
                        } elseif ($type === RdfNamespace::get("skos") . "ConceptScheme") {
                            $newURI = $tisafHost . $vocabId;
                            $schemesCounter++;
                        }
                    }
                    $uniqueSubjects[$subjectUri] = 1;
                    $queries[$subjectUri] = self::buildInsertTisafQuery($subjectUri, $newURI, $tisafUriProperty);
                }
            } else {
                error_log("Expected label of type Literal as value: $triple->label");
            }
        }
        return ["queries" => $queries, "conceptsCounter" => $conceptsCounter, "schemesCounter" => $schemesCounter, "expectedCount" => count($uniqueSubjects)];
    }

    private static function getCountForQuery($sparql, $query) {
        $result = $sparql->execute($query);
        $resultCount = -1;
        if (!empty($result) && $result[0] && $result[0]->count instanceof Literal\Integer) {
            $resultCount = $result[0]->count->getValue();
        }
        return $resultCount;
    }


    /**
     * Creates an executable sparql query string to update add a owl:sameAs statement to a
     * single triple (see $subjectUri) in a specific graph (see $graphName).
     *
     * @param $subjectUri
     * @param $newTisafUri
     * @return string
     */
    private static function buildInsertTisafQuery($subjectUri, $newTisafUri, string $tisafUriProperty) {
        //    old:     <$subjectUri> owl:sameAs <$newTisafUri>
        // tisaf prefix can be used (no full uri needed), because we set it globally in GlobalConfig.php
        // !!! property name must be same as in index.php - getOriginalIdentifierFromTisafUri() !!!
        return <<<EOD
        <$subjectUri> $tisafUriProperty <$newTisafUri>
EOD;
    }

    private static function transformUmlaute($s) {
        $s = str_replace ("ä", "ae", $s);
        $s = str_replace ("Ä", "Ae", $s);
        $s = str_replace ("ü", "ue", $s);
        $s = str_replace ("Ü", "Ue", $s);
        $s = str_replace ("ö", "oe", $s);
        $s = str_replace ("Ö", "Oe", $s);
        $s = str_replace ("ß", "ss", $s);
        return $s;
    }
}

#PREFIX owl: <http://www.w3.org/2002/07/owl#>
#INSERT DATA {
#   GRAPH $graphName {
#      <$subjectUri> owl:sameAs <$newTisafUri>
#  }
#}
#EOD;