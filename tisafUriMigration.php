<?php

require_once 'vendor/autoload.php';
require_once 'controller/TisafUriHelper.php';
header("Access-Control-Allow-Origin: *"); // enable CORS for the whole REST API

/**
 * this is a migration script to add rdf triples with a newly created tisaf-URI,
 * based on the *ENGLISH* label (skos:prefLabel, ...) for each concept of the vocabulary
 * specified by $vocabId (see below)
 */

try {
    TisafUriHelper::setTisafUrisForTerminology('ebuff2');
} catch (Exception $e) {
    header("HTTP/1.0 500 Internal Server Error");
    echo('ERROR: ' . $e->getMessage());
}